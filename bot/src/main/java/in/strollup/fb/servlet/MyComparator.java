package in.strollup.fb.servlet;


import java.util.Comparator;

public class MyComparator<T> implements Comparator<String[]> {

	@Override
	public int compare(String[] lhs, String[]  rhs) {
        return compare(Long.parseLong(rhs[3]),Long.parseLong(lhs[3]));
	}
    public int compare(long lhs, long rhs) {
        return lhs < rhs ? -1 : (lhs == rhs ? 0 : 1);
    }
}
