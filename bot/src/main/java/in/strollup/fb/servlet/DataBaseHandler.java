package in.strollup.fb.servlet;


import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import org.apache.commons.dbcp2.BasicDataSource;

public class DataBaseHandler
{
	static final String JDBC_DRIVER = "com.microsoft.sqlserver.jdbc.SQLServerDriver";
	static final String DB_URL = "jdbc:sqlserver://192.168.3.18:1433"+";databaseName=QMSSTAGING";
	static final String USER = "EmtStaging";
	static final String PASS = "Emtsa@123";
	private BasicDataSource ds_dbcp2;
	private static DataBaseHandler dbHandler = null;

	public DataBaseHandler()
	{
		ds_dbcp2 = new BasicDataSource();
	}

	public static DataBaseHandler getInstance()
	{
		if (dbHandler == null)
		{
			synchronized (DataBaseHandler.class)
			{
				if (dbHandler == null)
				{
					dbHandler = new DataBaseHandler();
					dbHandler.ds_dbcp2.setDriverClassName(JDBC_DRIVER);
					dbHandler.ds_dbcp2.setUrl(DB_URL);
					dbHandler.ds_dbcp2.setUsername(USER);
					dbHandler.ds_dbcp2.setPassword(PASS);
					dbHandler.ds_dbcp2.setInitialSize(10);
					dbHandler.ds_dbcp2.setMaxTotal(50);
					dbHandler.ds_dbcp2.setMaxIdle(50);
					dbHandler.ds_dbcp2.setMinIdle(50);
					dbHandler.ds_dbcp2.setMaxOpenPreparedStatements(180);

				}
			}
		}

		return dbHandler;
	}

	public Connection getDBCP2Connection() throws SQLException
	{
		return dbHandler.ds_dbcp2.getConnection();
	}

	public Connection getConnection() throws Exception
	{
		return this.getDBCP2Connection();
	}

	public void updateSessionChatCount(String channelId, String macthedChannelId) throws Exception
	{
		List<String[]> userDetails = readData(Constants.userDetailsQuery.replace("__channelId", channelId));
	    System.out.println("User Details"+userDetails.get(0)[4]);
		int sessionChatCount = Integer.parseInt(userDetails.get(0)[6]!=null?userDetails.get(0)[6]:"0");
		if (sessionChatCount == 99){
			updateTotalCoins(macthedChannelId, 1);
			updateTotalCoins(channelId, 1);}
		updateData(Constants.matchedUsersTable, new String[]
				{ "sessionChats" }, Arrays.asList(String.valueOf(sessionChatCount + 1)), "channelId='" + channelId+"'");
		updateData(Constants.matchedUsersTable, new String[]
				{ "sessionChats" }, Arrays.asList(String.valueOf(sessionChatCount + 1)), "channelId='" + macthedChannelId+"'");

	}

	public void updateTotalCoins(String channelId, int coins) throws Exception
	{
		List<String[]> userDetails = readData(Constants.userDetailsQuery.replace("__channelId", channelId));
		int totCoins = Integer.parseInt(userDetails.get(0)[6]);
		updateData(Constants.userDetailsTable, new String[]
				{ "totCoins" }, Arrays.asList(String.valueOf(totCoins+coins)), "channelId='" + channelId+"'");
	}

	public void addData(String tableName, String[] columnNames, List<String> data) throws Exception
	{
		Connection conn = getConnection();
		System.out.println("Adding data to Database. Table Name : " + tableName);
		System.out.println("Columns : " + Arrays.toString(columnNames));
		System.out.println("Values : " + Arrays.toString(data.toArray()));
		try
		{
			if (columnNames.length != data.size())
			{
				System.out.println("Number of columns does not match number of data values in a row");
				throw new Exception("Number of columns does not match number of data values in a row. Columns : " + Arrays.toString(columnNames) + " Values : " + data.toArray());
			} else
			{
				String sql = "INSERT INTO " + tableName + " ";
				sql += "(";
				for (String column : columnNames)
					sql += column + ",";
				sql = sql.substring(0, sql.length() - 1);
				sql += ") ";

				sql += "VALUES (";
				for (String value : data)
					sql += "?,";
				sql = sql.substring(0, sql.length() - 1);
				sql += ")";
                System.out.println(sql);
				PreparedStatement preparedStatement = conn.prepareStatement(sql);
				for (int i = 0; i < data.size(); i++)
					preparedStatement.setString(i + 1, data.get(i));

				// System.out.println(sql);
				preparedStatement.executeUpdate();
				// System.out.println("Inserted records into the table...");

			}
		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception e)
			{
				System.out.println("Exception"+ " ");
			}
		}

	}
	public String addData_advice(String tableName, String[] columnNames, List<String> data) throws Exception
	{
		Connection conn = getConnection();
		System.out.println("Adding data to Database. Table Name : " + tableName);
		System.out.println("Columns : " + Arrays.toString(columnNames));
		System.out.println("Values : " + Arrays.toString(data.toArray()));

		try
		{
			if (columnNames.length != data.size())
			{
				System.out.println("Number of columns does not match number of data values in a row");
				throw new Exception("Number of columns does not match number of data values in a row. Columns : " + Arrays.toString(columnNames) + " Values : " + data.toArray());

			} else
			{
				// Insert data in database

				// STEP 4: Execute a query
				// System.out.println("Inserting records into the table...");

				String sql = "INSERT INTO " + tableName + " ";

				sql += "(";
				for (String column : columnNames)
					sql += column + ",";
				sql = sql.substring(0, sql.length() - 1);
				sql += ") ";

				sql += "VALUES (";
				for (String value : data)
					sql += "?,";
				sql = sql.substring(0, sql.length() - 1);
				sql += ")";

				PreparedStatement preparedStatement = conn.prepareStatement(sql,Statement.RETURN_GENERATED_KEYS);
				for (int i = 0; i < data.size(); i++)
					preparedStatement.setString(i + 1, data.get(i));

				// System.out.println(sql);
				preparedStatement.executeUpdate();
				ResultSet rs = preparedStatement.getGeneratedKeys();
				if (rs.next()){
					int risultato=rs.getInt(1);
					System.out.println("kkkkkkkkk  "+risultato);
					return ""+risultato;
				}
				return "";

			}
		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception e)
			{
			}

		}

	}
	public void updateData(String tableName, String[] columnNames, List<String> data, String updateCriteria) throws Exception
	{
		Connection conn = getConnection();

		System.out.println("Updating data: Table Name : " + tableName);
		System.out.println("Columns : " + Arrays.toString(columnNames));
		System.out.println("Values : " + Arrays.toString(data.toArray()));
		System.out.println("Update criteria : " + updateCriteria);

		try
		{
			if (columnNames.length != data.size())
			{
				System.out.println("Number of columns does not match number of data values in a row");
				throw new Exception("Cannot create values map for zoho db operation. " + "Number of columns does not match number of data values in a row. Columns : " + Arrays.toString(columnNames)
				+ " Values : " + data.toArray());
			} else
			{
				// STEP 4: Execute a query
				// System.out.println("Creating statement...");
				String sql = "UPDATE " + tableName + " SET ";
				for (int i = 0; i < columnNames.length; i++)
				{
					sql += columnNames[i] + "=?";
					if (i != columnNames.length - 1)
						sql += ",";
				}
				sql += " WHERE " + updateCriteria;

				PreparedStatement preparedStatement = conn.prepareStatement(sql);
				for (int i = 0; i < data.size(); i++)
					preparedStatement.setString(i + 1, data.get(i));

				// System.out.println(sql);
				preparedStatement.executeUpdate();
			}

		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception e)
			{
			}
		}
	}

	public List<String[]> readData(String readQuery) throws Exception
	{

		Connection conn = getConnection();

		List<String[]> data = new ArrayList<String[]>();

		System.out.println("Read query : " + readQuery);

		Statement stmt = null;
		try
		{
			// STEP 4: Execute a query
			// System.out.println("Creating statement...");
			stmt = conn.createStatement();

			ResultSet rs = stmt.executeQuery(readQuery);

			// Extract data from resultset
			while (rs.next())
			{
				String[] row = new String[rs.getMetaData().getColumnCount()];
				int i = 0;
				while (i < rs.getMetaData().getColumnCount())
				{ // don't skip the last column, use <=
					row[i] = rs.getString(++i);
				}
				data.add(row); // add it to the result
			}

		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception e)
			{
			}
		}
		return data;
	}

	public List<String[]> readDataPreparedStatements(String readQuery, List<String> args) throws Exception
	{
		Connection conn = getConnection();

		List<String[]> data = new ArrayList<String[]>();

		try
		{
			PreparedStatement preparedStatement = conn.prepareStatement(readQuery);
			for (int i = 1; i <= args.size(); i++){
				System.out.println(i+"----"+args.get(i-1));
				preparedStatement.setString(i, args.get(i-1));
			}
			ResultSet rs = preparedStatement.executeQuery();

			// Extract data from resultset
			while (rs.next())
			{
				String[] row = new String[rs.getMetaData().getColumnCount()];
				int i = 0;
				while (i < rs.getMetaData().getColumnCount())
				{ // don't skip the last column, use <=
					row[i] = rs.getString(++i);
				}
				data.add(row); // add it to the result
			}

		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception e)
			{
			}
		}
		return data;
	}


	public boolean deleteData(String deleteQuery) throws Exception
	{
		Connection conn = getConnection();

		// STEP 4: Execute a query
		// System.out.println("Creating statement...");
		try
		{
			PreparedStatement preparedStatement = conn.prepareStatement(deleteQuery);
			preparedStatement.executeUpdate();

		} finally
		{
			try
			{
				if (conn != null)
					conn.close();
			} catch (Exception e)
			{
			}
		}

		return true;
	}

	public String getLikesByChannel(String channelid)
	{
		String query = "SELECT likes FROM " + "UserDetails" + " WHERE channelId='%s'";
		query = String.format(query, channelid);
		// System.out.println(query);
		return query;
	}

	public String getLikesNotByChannelid(String channelid, String timezone)
	{
		String query = "SELECT channelId, likes FROM " + "UserDetails" + " WHERE channelId !='%s' and " + "cast(timezone as decimal(10,6)) between cast('" + timezone
				+ "' as decimal(10,6)) -2 and cast('" + timezone + "' as decimal(10,6)) + 2 and "
				+ "channelId in (select channelId from UserState where state = 'waitingForMatch' and currPreference='m')";
		query = String.format(query, channelid);
		return query;
	}

	public String getLocationByChannel(String channelid)
	{
		String query = "SELECT checkIn FROM " + "UserDetails" + " WHERE channelId='%s'";
		query = String.format(query, channelid);
		// System.out.println(query);
		return query;
	}

	public String getLocationNotByChannelid(String channelid, String timezone)
	{
		String query = "SELECT channelId, checkIn FROM " + "UserDetails" + " WHERE channelId !='%s' and " + "cast(timezone as decimal(10,6)) between cast('" + timezone
				+ "' as decimal(10,6)) -2 and cast('" + timezone + "' as decimal(10,6)) + 2 and "
				+ "channelId in (select channelId from UserState where state = 'waitingForMatch' and currPreference='k')";
		query = String.format(query, channelid);
		return query;
	}

	public String getChatCount(String userId) throws Exception {
		List<String[]> userDetails = readData(Constants.userDetailsQuery.replace("__channelType","fb").replace("__channelId",userId));
		return userDetails.get(0)[6];
	}
	public void updateChatCount(String userId) throws Exception
	{
		List<String[]> userDetails = readData(Constants.userDetailsQuery.replace("__channelType","fb").replace("__channelId",userId));
		  updateData(Constants.userDetailsTable, new String[]
				{ "chatCount" }, Arrays.asList(String.valueOf(Integer.parseInt(userDetails.get(0)[6]) + 1)), "channelId='" + userId+"'");
	}




	/*public void throwMessage(String message,String user) 
	{
		try
		{
			JSONObject contextObj;
			String[] message2 = message.split("/");
			String query="";
			if(message2[1].equalsIgnoreCase("ALL"))
			{
				if(message2[2].equalsIgnoreCase("ALL"))
				{
					query="select channelId from UserDetails";
				}else {
					query="select channelId from UserDetails where timezone='"+message2[2]+"'";
				}
			}else {
				if(message2[2].equalsIgnoreCase("ALL"))
				{
					query="select channelId from UserDetails where gender='"+message2[1]+"'";
				}else {
					query="select channelId from UserDetails where gender='"+message2[1]+"' and timezone='"+message2[2]+"'";

				}
			}
			contextObj = new JSONObject("{\"channeltype\":\"fb\",\"contexttype\":\"p2p\",\"contextid\":\"__contextId\",\"botname\":\""+Constants.FACEBOOK.BOTNAME+"\"}");

			List<String[]> userDetails = this.readData(query);

			String apikey = Constants.FACEBOOK.APIKEY;
			String botname = Constants.FACEBOOK.BOTNAME;

			GupshupBotKit botkit = GupshupBotKit.getInstance(botname, apikey);
			GupshupBotAPI gsapi = botkit.gupshupAPI();
			//System.out.println("user size"+userDetails.size());
			gsapi.sendMessage("user size "+userDetails.size(), user);

			for (String[] data : userDetails)
			{
				gsapi.sendMessage(message2[3], contextObj.put("contextid", data[0].trim()));
				Thread.sleep(1000);
			}

		} catch (Exception e)
		{
			e.printStackTrace();
		}
	}
*/	/*public void sendMsg(String channelId,String msg) throws Exception
	{
		List<String[]> userDetails = readData(Handler.zohoDbProps.getProperty("user-details-query").replace("__channelId", channelId));
		int chatCount = Integer.parseInt(userDetails.get(0)[7]);
		updateData(Handler.zohoDbProps.getProperty("user-details-table"), new String[]
		{ "chatCount" }, Arrays.asList(String.valueOf(chatCount + 1)), "channelId=" + channelId);
	}*/

/*
 * 
 * 	public void updateChatCount(String channelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", channelId));
		int chatCount = Integer.parseInt(userDetails.get(0)[7]);
		updateData(WebHookServlet.config.getProperty("user-details-table"), new String[]
				{ "chatCount" }, Arrays.asList(String.valueOf(chatCount + 1)), "channelId='" + channelId+"'");
	}


	public void updateInvitedCount(String channelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", channelId));
		int invitedCount = Integer.parseInt(userDetails.get(0)[11]);
		updateData(WebHookServlet.config.getProperty("user-details-table"), new String[]
				{ "pplJoined" }, Arrays.asList(String.valueOf(invitedCount + 1)), "channelId='" + channelId+"'");
	}

	public void updateTotalCoins(String channelId, int coins) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", channelId));
		int totCoins = Integer.parseInt(userDetails.get(0)[10]);
		updateData(WebHookServlet.config.getProperty("user-details-table"), new String[]
				{ "totCoins" }, Arrays.asList(String.valueOf(totCoins + coins)), "channelId='" + channelId+"'");
	}

	public void updateWhomYouGave(String giverChannelId, String getterChannelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", giverChannelId));
		int pplGiven = Integer.parseInt(userDetails.get(0)[13]);
		updateData(WebHookServlet.config.getProperty("user-details-table"), new String[]
				{ "pplGiven" }, Arrays.asList(String.valueOf(pplGiven + 1)), "channelId='" + giverChannelId+"'");

		userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", getterChannelId));
		int pplGave = Integer.parseInt(userDetails.get(0)[12]);
		updateData(WebHookServlet.config.getProperty("user-details-table"), new String[]
				{ "pplGave" }, Arrays.asList(String.valueOf(pplGave + 1)), "channelId='" + getterChannelId+"'");
	}

	public boolean giveCoins(String giverChannelId, String getterChannelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", giverChannelId));
		int giverCoins = Integer.parseInt(userDetails.get(0)[10]);
		if (giverCoins < 10)
			return false;
		updateTotalCoins(giverChannelId, -10);
		updateTotalCoins(getterChannelId, 10);
		return true;

	}

	public String getCoins(String channelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", channelId));
		return userDetails.get(0)[10];
	}

	public String getGender(String channelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", channelId));
		return userDetails.get(0)[4];
	}

	public String getChatCount(String channelId,String botname) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelType", botname).replace("__channelId", channelId));
		return userDetails.get(0)[7];
	}

	public boolean updateInvitation(String userMessage, String channelId) throws Exception
	{
		List<String[]> inviteCodeOwnner = readData(WebHookServlet.config.getProperty("invite-code-query").replace("__inviteCode", userMessage).replace("__channelId", channelId));
		if (inviteCodeOwnner.size() > 0)
		{

			// Check If this channelId has used this invitation code before or
			// not
			List<String[]> isUsed = readData("select * from InviteCodeUsed where channelId = '" + channelId + "'");
			if (isUsed.size() > 0)
				return false;
			else
				dbHandler.addData("InviteCodeUsed", new String[]
						{ "channelId", "inviteCoded" }, Arrays.asList(channelId, userMessage));

			// /Update the people invited
			this.updateInvitedCount(inviteCodeOwnner.get(0)[0]);

			// Update total Coins
			//this.updateTotalCoins(inviteCodeOwnner.get(0)[0], 10);
			//this.updateTotalCoins(channelId, 10);

			return true;
		} else
		{
			return false;
		}
	}

	public float getHotPercentage(String channelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query").replace("__channelId", channelId));
		float hotPercentage;
		float notPercentage;
		if(userDetails.get(0)[17].equalsIgnoreCase("0"))
		{
			hotPercentage = (float) 50;
			notPercentage = 100 - hotPercentage;
		}else {
			hotPercentage = (float) 100 * Integer.parseInt(userDetails.get(0)[16]) / (Integer.parseInt(userDetails.get(0)[16]) + Integer.parseInt(userDetails.get(0)[17]));
			notPercentage = 100 - hotPercentage;
		}
		return hotPercentage;
	}

	public void updateHotNotCount(String userMessage, String channelId,String owner_channelId,String owner_name,String owner_url) throws Exception
	{
		if (userMessage.equalsIgnoreCase("hot") || userMessage.equalsIgnoreCase("not"))
		{
			String field = userMessage.equalsIgnoreCase("hot") ? "hotCount" : "notCount";
			List<String[]> userDetails = readData(WebHookServlet.config.getProperty("user-details-query2").replace("__channelId", channelId));
			int chatCount = Integer.parseInt(userDetails.get(0)[userMessage.equalsIgnoreCase("hot") ? 16 : 17]);
			updateData(WebHookServlet.config.getProperty("user-details-table"), new String[]
					{ field }, Arrays.asList(String.valueOf(chatCount + 1)), "channelId='" + channelId+"'");
			if(userMessage.equalsIgnoreCase("hot"))
			{
				String apikey = Constants.FACEBOOK.APIKEY;
				String botname = Constants.FACEBOOK.BOTNAME;

				GupshupBotKit botkit = GupshupBotKit.getInstance(botname, apikey);
				GupshupBotAPI gsapi = botkit.gupshupAPI();
				String shareDetailsMessage = Handler.zohoDbProps.getProperty("share-reply-message2").replace("__text1", owner_name).replace("__text2", owner_name+" rated you HOT, click on chat button to chat OR ignore").replace("__image", owner_url).replace("__url", Constants.FACEBOOK.BOTURL+"?ref=C_"+owner_channelId);
				JSONObject contextObj;

				try
				{
					contextObj = new JSONObject("{\"channeltype\":\"fb\",\"contexttype\":\"p2p\",\"contextid\":\"__contextId\",\"botname\":\""+Constants.FACEBOOK.BOTNAME+"\"}");


					gsapi.sendMessage(shareDetailsMessage, contextObj.put("contextid", userDetails.get(0)[0]));
				}catch(Exception e)
				{

				}

			}
		}

	}
	public void updateHotNotCount2(String userMessage, String id,String owner_channelId,String owner_name,String owner_url) throws Exception
	{
		if (userMessage.equalsIgnoreCase("hot") || userMessage.equalsIgnoreCase("not"))
		{
			System.out.println("l");

			String field = userMessage.equalsIgnoreCase("hot") ? "hotCount" : "notCount";
			System.out.println(Handler.zohoDbProps.getProperty("hot-not-query").replace("__id", id));
			List<String[]> userDetails = readData("select * from HotNotImage where id= "+id+" limit 1");
			int chatCount = Integer.parseInt(userDetails.get(0)[userMessage.equalsIgnoreCase("hot") ? 5 : 6]);
			updateData(Handler.zohoDbProps.getProperty("hot-not-image-table"), new String[]
					{ field }, Arrays.asList(String.valueOf(chatCount + 1)), "id=" + id);
			if(userMessage.equalsIgnoreCase("hot"))
			{
				System.out.println("lol");
				String apikey = Constants.FACEBOOK.APIKEY;
				String botname = Constants.FACEBOOK.BOTNAME;

				GupshupBotKit botkit = GupshupBotKit.getInstance(botname, apikey);
				GupshupBotAPI gsapi = botkit.gupshupAPI();
				String shareDetailsMessage = Handler.zohoDbProps.getProperty("share-reply-message2").replace("__text1", owner_name).replace("__text2", owner_name+" rate u as hot pls click on chat button to chat").replace("__image", owner_url).replace("__url", Constants.FACEBOOK.BOTURL+"?ref=C_"+owner_channelId);
				JSONObject contextObj;

				try
				{
					contextObj = new JSONObject("{\"channeltype\":\"fb\",\"contexttype\":\"p2p\",\"contextid\":\"__contextId\",\"botname\":\""+Constants.FACEBOOK.BOTNAME+"\"}");

					System.out.println("lol2");

					gsapi.sendMessage(shareDetailsMessage, contextObj.put("contextid", userDetails.get(0)[0]));
				}catch(Exception e)
				{

				}

			}
		}

	}

	public String updateAdviceLikesCount(String advice, String channelId) throws Exception
	{
		List<String[]> userDetails = readDataPreparedStatements("select * from Advices where id = ? limit 1", Arrays.asList(advice));
		//if (userDetails.get(0)[0].equalsIgnoreCase(channelId))return;
		int count = Integer.parseInt(userDetails.get(0)[2]);
		updateData("Advices", new String[]
				{ "likes" }, Arrays.asList(String.valueOf(count + 1)), "id=" + advice+"");
		return String.valueOf(count + 1);
	}

	public String updateOneLinerLikesCount(String oneLiner, String channelId) throws Exception
	{
		List<String[]> userDetails = readDataPreparedStatements("select * from OneLiners where id = ? limit 1", Arrays.asList(oneLiner));
		//if (userDetails.get(0)[0].equalsIgnoreCase(channelId))return;
		int count = Integer.parseInt(userDetails.get(0)[2]);
		updateData("OneLiners", new String[]
				{ "likes" }, Arrays.asList(String.valueOf(count + 1)), "id=" + oneLiner+"");
		System.out.println(String.valueOf(count + 1));
		return String.valueOf(count + 1);
	}

	public void updateAdviceGivenCount(String channelId) throws Exception
	{
		List<String[]> userDetails = readData(Handler.zohoDbProps.getProperty("user-details-query").replace("__channelId", channelId));
		int adviceGivenCount = Integer.parseInt(userDetails.get(0)[9]);
		updateData(Handler.zohoDbProps.getProperty("user-details-table"), new String[]
				{ "advice" }, Arrays.asList(String.valueOf(adviceGivenCount + 1)), "channelId='" + channelId+"'");
	}
	public static void main (String args[]){
		String text = "Hello \"World\"";
		System.out.println(text.replaceAll("\"", "'"));
	}

	public void updateSessionChatCount(String channelId, String macthedChannelId) throws Exception
	{
		List<String[]> userDetails = readData(WebHookServlet.config.getProperty("matched-users-query").replace("__channelId", channelId));
	    System.out.println("User Details"+userDetails.get(0)[4]);
		int sessionChatCount = Integer.parseInt(userDetails.get(0)[5]!=null?userDetails.get(0)[5]:"0");
		if (sessionChatCount == 99){updateTotalCoins(macthedChannelId, 1);updateTotalCoins(channelId, 1);}
		updateData(WebHookServlet.config.getProperty("matched-users-table"), new String[]
				{ "sessionChats" }, Arrays.asList(String.valueOf(sessionChatCount + 1)), "channelId='" + channelId+"'");
		updateData(WebHookServlet.config.getProperty("matched-users-table"), new String[]
				{ "sessionChats" }, Arrays.asList(String.valueOf(sessionChatCount + 1)), "channelId='" + macthedChannelId+"'");

	}*/
}
