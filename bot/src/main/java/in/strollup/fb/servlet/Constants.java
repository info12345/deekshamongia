package in.strollup.fb.servlet;

import java.io.IOException;
import java.util.Properties;

public class Constants {
	public static String googleMapApiKey="AIzaSyDW088HhLTJ-KswWmZb-3D6Zuxdoy0lPIM";
	public static String userStateQuery="select  TOP 1 * from tblFB_UserState where ChannelId='__channelId'";
	public static String products="select Name from tblFB_Products where Id='_id'";
	public static String welcomeMsg="Welcome to EaseMyTrip.\nType menu to start again.\nType end to discontinue anytime.\nWhat would you like to inquire about?";
	public static String existingBookingHeading="Choose your concern from the following options?";
	public static String agentConnectSubHeading="\nClick Register to register as a Local Travel Agent.\nClick Connect to connect with a Local Travel Agent.";
	public static String registertaQuery="select * from registertravelagent where channelId='__channelId' limit 1";
	public static String userStateTable = "tblFB_UserState";
	public static String noRespCols="ChannelId,ChannelType,Message,Error,Date";
	public static String userStateCols="ChannelId,ChannelType,State,CurrentPreference,UpdateTime,MatchId";
	public static String userDetailsQuery="select  TOP 1 * from tblFB_UserDetail where ChannelId='__channelId'";
	public static String citySelectionQuery="select ChannelId,City,Latitude,Longitude from tblFB_UserDetail where userType='6' and ProductId like '%dept%'";
	public static String registertravelagentQuery="select TOP 1 * from registertravelagent where channelId='__channelId'";
	public static String userDetailsTable="tblFB_UserDetail";
	public static String findLocalTA="select * from tblFB_UserState where state='waitingForMatch' and channelId='nearByAgent' order by updateTime";
	public static String userDetailsCols="ChannelId,ChannelType,ProfileName,ProfileImage,Gender,ChatCount,Country,ProductId,BussinessType,PageId,Mobile,IsVerifiedMobile,EmailId,userType,Latitude,Longitude";
	public static String matchFoundMessage="Amazing.. now start chatting with representative. Type END to discontinue anytime";
	public static String matchAgent="Amazing.. now start chatting with <customer>. Type END to discontinue anytime";
	public static String matchedUsersQuery="select TOP 1 * from tblFB_MatchUsers where ChannelId='__channelId'";
	public static String matchedUsersTable="tblFB_MatchUsers";
	public static String busyMessage="Oops! Our local agents are presently occupied attending other customers. You will be contacted shorty!";	 
	public static String registertravelagentTable="registertravelagent";
	public static String matchHistoryTable="tblFB_UserMatchHistory";
	public static String matchHistoryCols="channelId1,channelId2,matchType,matchtime";
	public static String registertravelagentCols="channelId,isMobileNo,isCity,isChannelIdAgent,isCityUpdatedAgent";
	public static String chatHistoryTable="tblFB_UserChatMessageHistory";
	public static String noRespTable="tblFB_NoResponse";
	public static String chatHistoryCols="MatchID,ChanelId1,ChanelId2,UserMessage,Messagetime";
	public static String matchedUsersCols="ChannelId,ChannelType,MatchedChannelId,MatchedChannelType,SessionChats,StartTime";
	public static String findCustomerCareQuery="select TOP 10 * from tblFB_UserState where State='waitingForMatch' and ChannelId!='__channelId' and ChannelId in (select ChannelId from tblFB_UserDetail where userType='3' and ProductId like '%dept%') order by updateTime";
	public static String findCustomerCareAgentQuery="select * from tblFB_UserState where state='waitingForMatch' and channelId!='__channelId' and channelId in (select channelId from tblFB_UserDetail where userType='client' and department='dept') order by updateTime  LIMIT 0, 10";
	public static String findLTA="select TOP 10* from tblFB_UserState where state='waitingForMatch' and channelId!='__channelId' and channelId in (select channelId from tblFB_UserDetail where city like '%_city%'  and userType='6' and ProductId like '%dept%' and BussinessType='B2B2C') order by updateTime";
	public static String findTravelAgentQuery="select * from tblFB_UserState where state='waitingForMatch' and channelId!='__channelId' and channelId in (select channelId from UserDetails where city in(_city) and userType='Customer') order by updateTime LIMIT 0, 10";
	public static String matchFoundMessage1="Amazing.. now start chatting with representative from Travel Agent department.Type END to discontinue anytime";
	public static String matchAgent1="Amazing.. now start chatting with customer.Type END to discontinue anytime";
	public static String waitingMessage="Please wait while we connect you to our travel consultant. We appreciate your patience.";	 
	public static String waitMsgLocTa="Please wait while we connect you to your local travel agent.We appreciate your patience";	 
	public static String usersQuery="select * from Users u INNER JOIN tblFB_ProductsRights pr ON pr.UserID = u.UserID where ChannelId='__channelId'";
	public static Properties loadPropertyFromClasspath(String fileName, Class<?> type) throws IOException
	{

		Properties prop = new Properties();
		prop.load(type.getClassLoader().getResourceAsStream(fileName));
		System.out.println(prop);
		return prop;

	}
	
}
