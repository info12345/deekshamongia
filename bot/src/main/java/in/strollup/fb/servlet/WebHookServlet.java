package in.strollup.fb.servlet;
import in.strollup.fb.contract.APIResponse;
import in.strollup.fb.contract.AttachmentCatelog;
import in.strollup.fb.contract.AttachmentFile;
import in.strollup.fb.contract.AttachmentLocation;
import in.strollup.fb.contract.AttachmentSend;
import in.strollup.fb.contract.Button;
import in.strollup.fb.contract.ButtonCatelog;
import in.strollup.fb.contract.CoordinatesLocation;
import in.strollup.fb.contract.ElementTemplate;
import in.strollup.fb.contract.EntryLocation;
import in.strollup.fb.contract.ExampleCatelog;
import in.strollup.fb.contract.ExampleFile;
import in.strollup.fb.contract.ExampleLocation;
import in.strollup.fb.contract.ExampleLocationQR;
import in.strollup.fb.contract.ExampleSend;
import in.strollup.fb.contract.Example_APIResponse;
import in.strollup.fb.contract.Example_Quick_Replies;
import in.strollup.fb.contract.Example_Text;
import in.strollup.fb.contract.MessageCatelog;
import in.strollup.fb.contract.MessageFile;
import in.strollup.fb.contract.MessageLocation;
import in.strollup.fb.contract.MessageLocationQR;
import in.strollup.fb.contract.MessageSend;
import in.strollup.fb.contract.Message_Quick_Reply;
import in.strollup.fb.contract.Message_Text;
import in.strollup.fb.contract.MessagingLocation;
import in.strollup.fb.contract.Payload;
import in.strollup.fb.contract.PayloadCatelog;
import in.strollup.fb.contract.PayloadFile;
import in.strollup.fb.contract.PayloadLocation;
import in.strollup.fb.contract.QuickReply;
import in.strollup.fb.contract.QuickReplyLocation;
import in.strollup.fb.contract.RecipientCatelog;
import in.strollup.fb.contract.RecipientFile;
import in.strollup.fb.contract.RecipientLocation;
import in.strollup.fb.contract.RecipientLocationQR;
import in.strollup.fb.contract.RecipientSend;
import in.strollup.fb.contract.RecipientTemplate;
import in.strollup.fb.contract.Recipient_Quick_reply;
import in.strollup.fb.contract.Recipient_Text;
import in.strollup.fb.contract.RequestModel;
import in.strollup.fb.contract.SenderLocation;
import in.strollup.fb.profile.FbProfile;
import okhttp3.FormBody;
import okhttp3.HttpUrl;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import org.apache.axis.wsdl.symbolTable.Utils;
import org.apache.commons.httpclient.util.URIUtil;
import org.apache.commons.lang.StringEscapeUtils;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.math.BigDecimal;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.stream.Collectors;
import java.util.*;
import javax.jws.WebService;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.soap.MessageFactory;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPBody;
import javax.xml.soap.SOAPConnection;
import javax.xml.soap.SOAPConnectionFactory;
import javax.xml.soap.SOAPElement;
import javax.xml.soap.SOAPEnvelope;
import javax.xml.soap.SOAPMessage;
import javax.xml.soap.SOAPPart;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.stream.StreamResult;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.HttpClientBuilder;
import org.datacontract.schemas._2004._07.AirService.AgentDetails;
import org.datacontract.schemas._2004._07.AirService.Authentication;
import org.datacontract.schemas._2004._07.AirService.LogOnRes;
import org.datacontract.schemas._2004._07.AirService.SubUserType;
import org.datacontract.schemas._2004._07.AirService.TransactionResponse;
import org.datacontract.schemas._2004._07.AirService.TransactionType;
import org.datacontract.schemas._2004._07.AirService.User;
import org.datacontract.schemas._2004._07.AirService.UserType;
import com.google.code.geocoder.Geocoder;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.xml.bind.v2.runtime.unmarshaller.XsiNilLoader.Array;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.XML;
import org.tempuri.AgencyRegistrationResponse;
import org.tempuri.BasicHttpBinding_IEmtTransactionServiceStub;
import org.tempuri.GetBotChatRegistrationDetailsResponse;
import org.tempuri.GetBotChatRegistrationDetailsResponseGetBotChatRegistrationDetailsResult;
import org.tempuri.IEmtTransactionService;
import org.tempuri.IEmtTransactionServiceProxy;
import org.tempuri.IReadTransactionServiceProxy;

@WebServlet("/webhook")
public class WebHookServlet extends HttpServlet 
  {
	public static int j=0,count=1,counting=1;
	private static final long serialVersionUID = -2326475169699351010L;
    Map<String,String> map=new HashMap<String,String>(); 
    private static final String VERIFY_TOKEN = "xyz";
	private static final HttpClient client = HttpClientBuilder.create().build();
    private static final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	private String profileLink,dept,deptClient,name,email,mobile,countryC,product,cityCus;
	private String welcome_msg,channelId,userId;
	RequestModel test1;
	private boolean flag,isCustomerCare=false,isMobileNo=false;
	private boolean isMatchingCalled=true;	
	private boolean isCity=false,isMobile=false,isMobileNoAgent=false,isEmailOrMobile=false,isCityCustomer=false,isParagraph=false,isConfirmationNo=false,isVerified=false,isContactNo=false,isEmailaddress=false,isEmailadd=false,isContactNoVerified=false,isEmailaddressAgent=false,isEmailaddressCustomer=false;
	private int	isVerifiedNo=0,isCityUpdatedAgent=0,isChannelIdAgent=0,isWaitMsgSent=0;
	String FB_MSG_URL,test,pincode,agentCountry,city,state,agentAddress,paragraph1,dob1,mobile1,email1,birthday,mobileCust,emailCust,countryCust,productCust,cityCust,latCust,longiCust;
	HttpServletResponse response;

	@Override
	protected void doPost(HttpServletRequest request,HttpServletResponse response) throws ServletException, IOException,NullPointerException 
	{
		String test = request.getReader().lines().collect(Collectors.joining(System.lineSeparator()));
        System.out.println("testing body  "+test);
	    Gson gson = new GsonBuilder().setPrettyPrinting().create();
		test1 = gson.fromJson(test,RequestModel.class);
		String jsonEmp = gson.toJson(test1);
		String text = null;
		count=1;	
   		String pageId=test1.getEntry().get(0).getId();
		  map.put("1156653614453520","EAASd5S9MQzUBALzyAwaZB8CsXZBcDHaa9iqXlzWc5Y2ZBZC7Gu9uUnAPliuFZCiXhyZBhX83ubJ6SZCVta0stadwh83I5JV9QKgepmbxdmb3Jfe5an840Cx8FfFz9k0W5GZCHoTuDPkFs0SVOcY36mmRDEZB2fwpsPbCmJvFwYLhW8gZDZD");  
		  map.put("1273407086039524","EAASd5S9MQzUBAAmKAgX07W7Ohk1OnMvgyj2UESuuEXEatIhpoUZBe3XoS8A8rhHFRfXlWPwyZA4LKxsjToDH6UKUzPmzFZAhQXzvaHdHzUfHTRInZAw7TDKxZCOLespBz67QcAgZCxlla0gLpB7ZBSpUwZAKvRvCvnW6kROaN8nODAZDZD");  
		  map.put("1642114346089551","EAASd5S9MQzUBAAPB0Ology3sbtBfAHo3xGYICX1cZAALb7DchMrpbT2vXSJYYxqNU3tABpZBZCEesjiPkWb1u1p0NeTlLUVRXxtjJNaq1ZAGywHxvQ9tW53pVvOlNE3gTrXZBt2kVumfSeAyyRMLxPQGm3T5VJ46CVJJeZAtBpewZDZD");  
		  map.put("1189538281112753","EAASd5S9MQzUBAKj2M4aV17qsAHFqkqNKWAkgWNIteArfQZAP5sa0hS3mdVCYJvZAcY8ZABP6qQH89bdgpHQZBSWpXcqLZAhQrOvsEI4yhFZC4PV4gvRcyUbWmZBh4xOHtWX0khF23ZCJ1XodGZA91lZALKFvZAquD0H6GlcGRE48lb3wgZDZD");  
		  map.put("1891454507744378","EAASd5S9MQzUBACrB5KL1DZBYDTaQRvnPkHlRpMhxkvOE0GlZACBFDndcOqfwiJ49kMtZAAUC1qiyub907zeE0as6rV2wAaZCZCDy5mE8rZB61Tp8SgPHC770kmRs6ETuj7VxtEyZBllZCZAra4q5WilcyfiaF9ZCVlVLBATC3hkEWdSQZDZD");  
		  map.put("771960196290137","EAASd5S9MQzUBAHZCg50o5DyRZCERc75NrtuaBODQNmQOoFBHR9YLr5XFJg2Y4kH52aLt0AzrOt6f22yXYsKvLNhiIpcdVpOJhpc6LnHunIN2tZACXqIiHE55VPEMPFazNsPqzQcXjod9HW2SHyipk6xx4YUE883S3ElZA5OrqgZDZD");  
		  String pageToken=map.get(pageId);
          System.out.println("body  "+pageToken);
          String FB_MSG_URL = "https://graph.facebook.com/v2.6/me/messages?access_token=";
		  FB_MSG_URL= FB_MSG_URL;
		  profileLink= "https://graph.facebook.com/v2.6/SENDER_ID?access_token=";
		  try {
		    text=" ";
			RequestModel model= gson.fromJson(test,RequestModel.class);
			if((model.getEntry().get(0).getMessaging().get(0).getMessage())!=null&&(model.getEntry().get(0).getMessaging().get(0).getMessage().getText())!=null)
			{				
		       text=model.getEntry().get(0).getMessaging().get(0).getMessage().getText();
		       text=text.trim();
		       System.out.println("TEXt.........."+text);	
		        }
			processRequest1(test1,test,response,FB_MSG_URL,profileLink);
		   	  
		  }
		 catch (Exception e) {
			 logExceptionInDB(userId,"fb",text,e);
			e.printStackTrace();
		}
	}
    
	@Override
	public void doGet(HttpServletRequest request, HttpServletResponse response)throws ServletException,IOException 
	{
		System.out.println("msg");
		String queryString = request.getQueryString();
		String msg = "Error, wrong token";
	
		if (queryString != null) {
			String verifyToken = request.getParameter("hub.verify_token");
			String challenge = request.getParameter("hub.challenge");
			if (StringUtils.equals(VERIFY_TOKEN, verifyToken)
					&& !StringUtils.isEmpty(challenge)) {
				msg = challenge;
			} else {
				msg = "";
			}
		} else {
			System.out.println("Exception no verify token found in querystring:"
							+ queryString);
		}
		response.setContentType("text/plain");
		System.out.println(msg);
		response.getWriter().write(msg);
		response.getWriter().flush();
		response.getWriter().close();
		response.setStatus(HttpServletResponse.SC_OK);
		return;
	}
	
	public String getQueryRefrenceNo(String name,String mobile,String email,String country,String product)
	{
		String result="";
		 String queryNo=" ";
		try
		{
			final OkHttpClient HTTP_CLIENT;
			final Gson GSON;
			HTTP_CLIENT = new OkHttpClient();
			GSON = new Gson();
			HttpUrl.Builder urlBuilder = HttpUrl.parse("http://qmsapi.easemytrip.com/service/QMSFBAPI.asmx/CreateNewQuery").newBuilder();
			urlBuilder.addQueryParameter("Name_",name);
			urlBuilder.addQueryParameter("mobile_",mobile);
			urlBuilder.addQueryParameter("email_",email);
			urlBuilder.addQueryParameter("type_","B2C_D");
			urlBuilder.addQueryParameter("productType_",product);
			urlBuilder.addQueryParameter("country_",country);
			String url = urlBuilder.build().toString();
			Request request = new Request.Builder().url(url).addHeader("Content-Type","application/x-www-form-urlencoded").build();
			okhttp3.Response response = HTTP_CLIENT.newCall(request).execute();
			final int code = response.code();
         	if (code == 200) 
         	{
				System.out.println("WORK!");
				queryNo=response.body().string();
				System.out.println("WORK!"+queryNo);
						}
         	else {
				System.out.println("not WORK!");
				System.out.println("ERROR: " + response.body().string());
			}
		}
		catch(Exception e)
		{
			e.printStackTrace();
			logExceptionInDB(userId,"fb","In Query Reference no Api",e);
		}
		return queryNo;
	}
	public String getQueryRefNoTA(String name,String mobile,String email,String country,String product,String city,String lat,String longi)
	{
		System.out.println("272");
		String result="";
		 String queryNo=" ";
		try
		{
			final OkHttpClient HTTP_CLIENT;
			final Gson GSON;
			HTTP_CLIENT = new OkHttpClient();
			GSON = new Gson();
			HttpUrl.Builder urlBuilder = HttpUrl.parse("http://qmsapi.easemytrip.com/Service/QMSFBAPI.asmx/CreateB2B2C_NewQuery").newBuilder();
			urlBuilder.addQueryParameter("Name_",name);
			urlBuilder.addQueryParameter("mobile_",mobile);
			urlBuilder.addQueryParameter("email_",email);
			urlBuilder.addQueryParameter("type_","B2B2C_D");
			urlBuilder.addQueryParameter("productType_",product);
			urlBuilder.addQueryParameter("country_",country);
			urlBuilder.addQueryParameter("agentCity_",product);
			urlBuilder.addQueryParameter("latitude_",lat);
			urlBuilder.addQueryParameter("longitude_",longi);
			String url = urlBuilder.build().toString();			
			Request request = new Request.Builder().url(url).addHeader("Content-Type","application/x-www-form-urlencoded").build();
			okhttp3.Response response = HTTP_CLIENT.newCall(request).execute();
			final int code = response.code();
         	if (code == 200) 
         	{
				System.out.println("WORK!");
				queryNo=response.body().string();
				System.out.println("WORK!"+queryNo);
						}
         	else {
				System.out.println("not WORK!");
				System.out.println("ERROR: " + response.body().string());
			}
		}
		catch(Exception e)
		{
			logExceptionInDB(userId,"fb","getQueryRefNoTA",e);
			e.printStackTrace();
		}
		return queryNo;
	}
	public String getAgentDetails()
	{
		String result="";
		String agentDetails=" ";
		 try
		{		
			Authentication auth=new Authentication();
			auth.setPassword("B2BaPPlication");
			User user=new User("B2B");
			auth.setUser(user);
			auth.setUserName("B2B");
			UserType userType= new UserType("B2B");
			auth.setUserType(userType);
			IReadTransactionServiceProxy proxy= new IReadTransactionServiceProxy();
			proxy.setEndpoint("http://dboperationstest.easemytrip.com/ReadTransactionService.svc?wsdl");
			System.out.println("293"+proxy.getEndpoint());	
			GetBotChatRegistrationDetailsResponseGetBotChatRegistrationDetailsResult transRes=proxy.getBotChatRegistrationDetails(auth);   
			GetBotChatRegistrationDetailsResponse response= new GetBotChatRegistrationDetailsResponse();
			response.setGetBotChatRegistrationDetailsResult(transRes);
			transRes=response.getGetBotChatRegistrationDetailsResult();
			if(transRes!=null)
				for(int i=0;i<response.getGetBotChatRegistrationDetailsResult().get_any().length;i++){
			        agentDetails=response.getGetBotChatRegistrationDetailsResult().get_any()[i].getAsString();
				}

			}
		catch(Exception e)
		{
			logExceptionInDB(userId,"fb","getAgentDetails",e);
			System.out.println("341");
			e.printStackTrace();
		}
		return agentDetails;
	}
	public String registerTravelAgent(String name,String mobile,String email,String city,String channelId,String country,String paragraph,String lat,String longi)
	{
		String result="";
		 String queryNo=" ";
		 try
		{
			counting++;
			AgentDetails agtDet=new AgentDetails();	
			agtDet.setBotAgentName(name);
			agtDet.setCountry(country);
			agtDet.setMobileNo(mobile);
			agtDet.setEmailId(email);
			agtDet.setCity(city);
			agtDet.setChannelId(channelId);
			agtDet.setBotAgentDetail(paragraph);
			agtDet.setLatitude(lat);
			agtDet.setLongitude(longi);
			Authentication auth=new Authentication();
			auth.setPassword("B2BaPPlication");
			User user=new User("B2B");
			auth.setUser(user);
			auth.setUserName("B2B");
			UserType userType= new UserType("B2B");
			auth.setUserType(userType);
			IEmtTransactionServiceProxy proxy= new IEmtTransactionServiceProxy();
			proxy.setEndpoint("http://dboperationstest.easemytrip.com/EmtTransactionService.svc?wsdl");
			System.out.println("293"+proxy.getEndpoint());	
			//BasicHttpBinding_IEmtTransactionServiceStub transService=new BasicHttpBinding_IEmtTransactionServiceStub();
			TransactionResponse transRes=proxy.chatBotRegistration(auth,agtDet);
			if(transRes.getTransactionStatus()!=null){
			transRes.getError();
			transRes.getMessage();
			transRes.getTransactionStatus();
			System.out.println(transRes.getError()+transRes.getMessage()+transRes.getTransactionStatus());
			boolean isWaitingMessageSent=false;
			FB_MSG_URL = "https://graph.facebook.com/v2.6/me/messages?access_token=";	
			String pageId=test1.getEntry().get(0).getId();			
			String url=FB_MSG_URL+map.get(pageId);		
			if(transRes.getMessage().equalsIgnoreCase("Fail Registration"))
			sendText(userId,url,"You are already registered");
			else
			sendText(userId,url,transRes.getMessage());	
				}
		}catch(Exception e)
		{
			logExceptionInDB(userId,"fb","registerTravelAgent",e);
			e.printStackTrace();
		}
		return queryNo;
	}
	
	public String getExistingDetails(String confirmationId,String bussinessType)
	{
		String result="";
		 String detail=" ";
		try
		{
			final OkHttpClient HTTP_CLIENT;
			final Gson GSON;
			HTTP_CLIENT = new OkHttpClient();
			GSON = new Gson();
			HttpUrl.Builder urlBuilder = HttpUrl.parse("http://qmsapi.easemytrip.com/service/QMSFBAPI.asmx/GetExistingBookingNumber").newBuilder();
			urlBuilder.addQueryParameter("_confirmationID",confirmationId);
			urlBuilder.addQueryParameter("_bussinessType",bussinessType);
			String url = urlBuilder.build().toString();
			Request request = new Request.Builder().url(url).addHeader("Content-Type","application/x-www-form-urlencoded").build();
			okhttp3.Response response = HTTP_CLIENT.newCall(request).execute();
			final int code = response.code();
         	if (code == 200) 
         	{
				System.out.println("WORK!");
				detail=response.body().string();
				System.out.println("WORK!"+detail);
						}
         	else {
				System.out.println("not WORK!");
				System.out.println("ERROR: " + response.body().string());
			}
		}catch(Exception e)
		{
			logExceptionInDB(userId,"fb","getExistingDetails",e);
			e.printStackTrace();
		}
		return detail;
	}
	public String myChannelId() 
	{
		return "channelId";
	}
	
	public static synchronized void logExceptionInDB(String channelId,String channelType,String message,Exception e)
	{
		StringWriter sw = new StringWriter();
		e.printStackTrace(new PrintWriter(sw));
		String[] columns=Constants.noRespCols.split(",");
		List<String> dataValues = new ArrayList<String>();
		dataValues.add(channelId);
		dataValues.add(channelType);
		dataValues.add(message);
		dataValues.add(sw.toString());
		dataValues.add(dateFormat.format(new Date()));
		try
		{
			DataBaseHandler.getInstance().addData(Constants.noRespTable,columns,dataValues);
		} catch (Exception e1)
		{
	
			e1.printStackTrace();
		}
	}
	
	
	
	private void processRequest1(RequestModel test1,String test,HttpServletResponse response,String FB_MSG_URL,String profileLink) throws Exception 
	{
		System.out.println("136");
		String text=" ";
		List<String[]> userDetails=new ArrayList(); 
		List<String[]> users=new ArrayList();
		String pageId=test1.getEntry().get(0).getId();
		String pageToken=map.get(pageId);
		boolean isWaitingMessageSent=false;
		FB_MSG_URL = "https://graph.facebook.com/v2.6/me/messages?access_token=";
        FB_MSG_URL= FB_MSG_URL;
		String payload=" ",firstName=" ",lastName=" ",profilePic=" ",locale=" ",gender=" ",timeZone=" ",country=" ";
		profileLink= "https://graph.facebook.com/v2.6/SENDER_ID?access_token=";
		String url=FB_MSG_URL+map.get(pageId);	
	    userId=test1.getEntry().get(0).getMessaging().get(0).getSender().getId();
	  //  nearByAgent(userId);
		List<String> userDataValues=new ArrayList();
		profileLink=profileLink+map.get(pageId);
		String link = StringUtils.replace(profileLink, "SENDER_ID",userId);
		System.out.println("Link"+link);
		String userStateQuery = Constants.userStateQuery.replace("__channelId",userId);
		FbProfile profile = getObjectFromUrl(link, FbProfile.class);
		  try{
			  if((test1.getEntry().get(0).getMessaging().get(0).getPostback())!=null){
			       payload=test1.getEntry().get(0).getMessaging().get(0).getPostback().getPayload();	
			       System.out.println("Payload.........."+payload);
			  }
		}
		catch(Exception e){
		 	payload=" ";
			text=" ";
			logExceptionInDB(userId,"fb",payload,e);
			e.printStackTrace();
		}
			try{
				if((test1.getEntry().get(0).getMessaging().get(0).getMessage())!=null&&(test1.getEntry().get(0).getMessaging().get(0).getMessage().getText())!=null){
			       text=test1.getEntry().get(0).getMessaging().get(0).getMessage().getText();
			       text=text.trim();
			       System.out.println("TEXt.........."+text);
					
				}
			}
			catch(Exception e){
				e.printStackTrace();
				logExceptionInDB(userId,"fb",text,e);
			}
			List<String[]> userStateReportData = DataBaseHandler.getInstance().readData(userStateQuery);
		    System.out.println("170"+userStateReportData.size()+text);
		    System.out.println("170"+userStateReportData.size()+text);
   		   if(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch")&&!text.equalsIgnoreCase("Agent Connect")&&!text.equalsIgnoreCase("Existing Booking")&&userStateReportData.get(0)[4].equalsIgnoreCase("chat")&&!text.isEmpty())
			 {
   			     System.out.println("263.............");
		    	 userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId));
				 if(((test1.getEntry().get(0).getMessaging().get(0).getMessage())!=null)&&((test1.getEntry().get(0).getMessaging().get(0).getMessage().getText())!=null)){
						text=test1.getEntry().get(0).getMessaging().get(0).getMessage().getText();
						System.out.println("text"+text);
						text=text.trim();
					    }
				 System.out.println(text);
				if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))
				 {
					 if(pageId.equalsIgnoreCase("1156653614453520"))
                     {
                     if(text.equalsIgnoreCase("GetChannelId"))
     				 {
                     	 sendText(userId,url,"Your channelID is"+userId);
     				 }
                     if(userDetails.size()>0&&userDetails.get(0)[15].equalsIgnoreCase("6"))
                     {
                    	  System.out.println("331.............");
          		    	
                    	 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                    	 sendText(userId,url,"Welcome to EaseMyTrip.");
           		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId
     							+ "' and channelType='" + "fb" + "'");
        				 return; 
                     }
                     
                     else{	 
                    	System.out.println("341.............");
          		    	sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                     	String title[]=new String[3];	
              		    title[0]="New Booking";
              		    title[1]="Existing Booking";
              		    title[2]="Agent Connect";
              		    String urls[]=new String[3];	
              		    urls[0]="http://i65.tinypic.com/4zwahe.png";
              		    urls[1]="http://i66.tinypic.com/35b9sfl.png";
              		    urls[2]="http://i65.tinypic.com/w9ziup.png";
              		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
              		 DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("welcomeMessageSent", "none",dateFormat.format(new Date())), "channelId='" + userId
  							+ "' and channelType='" + "fb" + "'");
     				
              		   return;
                	}
                     }
					 else{
						 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                    	 sendText(userId,url,"Welcome to EaseMyTrip.");
           		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId
     							+ "' and channelType='" + "fb" + "'");
        				 return; 
					 }
					 
				}
				 else if(userDetails.get(0)[15].equalsIgnoreCase("3")||userDetails.get(0)[15].equalsIgnoreCase("6"))
				 {
					 System.out.println(text+"549.....");	
					 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                	 sendText(userId,url,"Welcome to EaseMyTrip.");		
                	 return; 	 
				 }
				 else if(! (payload!=null&&payload.contains("startchattingevent"))) {
					 System.out.println(text+"339.....");	
					 isWaitingMessageSent=true;
					 sendText(userId,url,Constants.waitingMessage);	
					 return; 
				 }
				
				
			 }
		    if(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("matched"))
          	{
		    	System.out.println("571.............");
		    	chattingMethod(test1,test,response,FB_MSG_URL,profileLink);
			    	return;
          	}
		   if(text.equalsIgnoreCase("Flights")&&(userStateReportData.size()>0)&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
           	{
			 productCust= "Flights"; 
           	 System.out.println("224...");
			 DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]{"ProductId"}, Arrays.asList("1"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");          	 
           	 sendText(userId,url,"We would like to take few details before connecting you to one of our travel consultant.");       	 
           	 sendText(userId,url,"What is your mobile number?");    
           	 isMobileNo=true;
             return;
           		 }			   
             else if(text.equalsIgnoreCase("Hotels")&&(userStateReportData.size()>0)&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))	
         	{
            productCust= "Hotels";                 	 	 
            System.out.println("235...");	 
 			DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]{"ProductId"}, Arrays.asList("2"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
         	 sendText(userId,url,"We would like to take few details before connecting you to one of our travel consultant.");       	 
         	 sendText(userId,url,"What is your mobile number?");  
         	 isMobileNo=true;
              return;
         		 }
             else if(text.equalsIgnoreCase("Holidays")&&(userStateReportData.size()>0)&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))	
          	{
             productCust= "Holidays";                 	 	 
             System.out.println("245...");            	 
 			 DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
 											{"ProductId"}, Arrays.asList("3"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
          	 sendText(userId,url,"We would like to take few details before connecting you to one of our travel consultant.");       	 
          	 sendText(userId,url,"What is your mobile number?");  
          	 isMobileNo=true;
               return;
          		 }		
 			 
		   if(isMobileNo)
 			{
 				 System.out.println("255..."); 
 				if(text.matches("[1-9][0-9]{9,14}")){
 					isMobileNo=false;
 					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]{"Mobile","IsVerifiedMobile"}, Arrays.asList(text,"1"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");		
 					userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId)
 							.replace("__channelType","fb"));
 					System.out.println("388"+userDetails.get(0)[6]);
 					if(userDetails.get(0)[9].equalsIgnoreCase("B2C"))
 					isEmailaddress=true; 
 					else if(userDetails.get(0)[9].equalsIgnoreCase("B2B2C")){
 					isEmailaddressCustomer=true; 
 					mobileCust=text;
 					System.out.println("Email393"+isEmailaddressCustomer);
 					}
 					sendText(userId,url,"What is your email address?");
 				return;
 				}
 				 else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end")){
 					isMobileNo=false;
 					 System.out.print("267.................");
 		    		DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
 		    		sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                 	String title[]=new String[3];	
          		    title[0]="New Booking";
          		    title[1]="Existing Booking";
          		    title[2]="Agent Connect";
          		    String urls[]=new String[3];	
          		    urls[0]="http://i65.tinypic.com/4zwahe.png";
          		    urls[1]="http://i66.tinypic.com/35b9sfl.png";
          		    urls[2]="http://i65.tinypic.com/w9ziup.png";
          		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
              	     return;		
 				 }
 				else {
 					sendText(userId,url,"Please enter valid mobile number.");
 					//isMobileNo=false;
 					return;
 				} 				
             }
		else if((isEmailaddressCustomer&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))))	{
            	System.out.println("284.......");
            	if(isValidEmailAddress(text))
 				{
            		emailCust=text;
            		System.out.println("366.........");
 					isEmailaddressCustomer=false;
 					isEmailadd=true;
 					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
 							{"EmailId"}, Arrays.asList(text), "channelId='" +userId + "' and channelType='"+"fb"+ "'");	
 					userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId)
  							.replace("__channelType","fb"));		
 					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 							{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
 					name=userDetails.get(0)[3];
 					emailCust=userDetails.get(0)[13];
					mobileCust=userDetails.get(0)[11];
					countryC=userDetails.get(0)[7];
					productCust=userDetails.get(0)[8];
					cityCust=userDetails.get(0)[16];
					latCust=userDetails.get(0)[18];
					longiCust=userDetails.get(0)[19];
					if(productCust.equalsIgnoreCase("1")){
						product="FLT";
					}
                    if(productCust.equalsIgnoreCase("2")){
                    	product="HTL";
					}
                    if(productCust.equalsIgnoreCase("3")){
                    	product="HOL";
					} 					
 					System.out.println(name+"\n"+mobileCust+"\n"+emailCust+"\n"+countryC+"\n"+product+"\n"+cityCust+"\n"+latCust+"\n"+longiCust); 					
 					String s=getQueryRefNoTA(name,mobileCust,emailCust,countryC,product,cityCust,latCust,longiCust);
 					System.out.println("682............"+s);
 					s=XML.toJSONObject(s).toString();
					System.out.println("344............"+s);
					JSONObject jsonObj = new JSONObject(s);
					JSONObject c = jsonObj.getJSONObject("HolidayResponse");
					String content = c.getString("Status");
					System.out.println(content);
					if(content.equalsIgnoreCase("true"))
					{
					  String queryNo = c.getString("QueryNumber");					
					  sendText(userId,url,"Your query number is "+queryNo);
					}
					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
							{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
					System.out.println("696.................");
					chattingMethod(test1,test,response,FB_MSG_URL,profileLink);					
 					return;
 				}
            	else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))
					 {
			    		 System.out.print("416.................");
			    		 DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
									{"BussinessType","userType"}, Arrays.asList("B2C","3"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
			    		DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
	 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
			    		String title[]=new String[3];	
	         		    title[0]="New Booking";
	         		    title[1]="Existing Booking";
	         		    title[2]="Agent Connect";
	         		    String urls[]=new String[3];	
	         		    urls[0]="http://i67.tinypic.com/smr88i.png";
	         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
	         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
	         		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");    
					    System.out.println("425.................");
					    return;
					 }
					else
					{
						System.out.println("423.......");
						sendText(userId,url,"Please enter valid email address.");
						return;
					}                   
		       }
	 	else if((isEmailaddress&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))))	
     	{
           	System.out.println("467.......");
           
			if(isValidEmailAddress(text))
				{
			System.out.println("471.........");
					if(isEmailaddress){
					isEmailaddress=false;
					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{"EmailId"}, Arrays.asList(text), "channelId='" +userId + "' and channelType='"+"fb"+ "'");	
					userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId)
 							.replace("__channelType","fb"));		
					name=userDetails.get(0)[3];
					email=userDetails.get(0)[13];
					mobile=userDetails.get(0)[11];
					countryC=userDetails.get(0)[7];
					product=userDetails.get(0)[8];
					System.out.println("358..........."+name+" "+email+" "+mobile+" "+countryC+" "+product);
					if(product.equalsIgnoreCase("1")){
						product="FLT";
					}
                    if(product.equalsIgnoreCase("2")){
                    	product="HTL";
					}
                    if(product.equalsIgnoreCase("3")){
                    	product="HOL";
					}
					String s=getQueryRefrenceNo(name,mobile,email,countryC,product);
					s=XML.toJSONObject(s).toString();
					System.out.println("344............"+s);
					//Example_APIResponse ex=new Example_APIResponse();
					JSONObject jsonObj = new JSONObject(s);
					JSONObject c = jsonObj.getJSONObject("string");
					String content = c.getString("content");
					System.out.println(content);
					if(content!=null)
					sendText(userId,url,"Your query number is "+content);
					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
							{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
    				}		
					System.out.println("783.................");
					chattingMethod(test1,test,response,FB_MSG_URL,profileLink);
					return;
				}
				 else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end")){
		    		 System.out.print("510.................");
		    		 DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
								{"BussinessType","userType"}, Arrays.asList("B2C","3"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
		    		DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
		    		String title[]=new String[3];	
         		    title[0]="New Booking";
         		    title[1]="Existing Booking";
         		    title[2]="Agent Connect";
         		    String urls[]=new String[3];	
         		    urls[0]="http://i67.tinypic.com/smr88i.png";
         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
         		    sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");				    
				    System.out.println("425.................");
				    return;
				 }
				 else{
					System.out.println("423.......");
					sendText(userId,url,"Please enter valid email address.");
					return;
				}
			}
 		 if(isMobileNoAgent&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch")))
 			{
 				 System.out.println("460..."); 
 				if(text.matches("[1-9][0-9]{9,14}")){
 					isMobileNoAgent=false;
 					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]{"Mobile","IsVerifiedMobile"}, Arrays.asList(text,"1"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");		
 					userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId)
 							.replace("__channelType","fb"));
 					isEmailaddressAgent=true; 
 					mobile1=text;
 					sendText(userId,url,"What is your email address?");
 				return;
 				}
 				 else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))
 				 {
 					isMobileNoAgent=false;
 					 System.out.print("472.................");
 		    		DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
 		         	   String title[]=new String[3];	
	         		    title[0]="New Booking";
	         		    title[1]="Existing Booking";
	         		    title[2]="Agent Connect";
	         		    String urls[]=new String[3];	
	         		    urls[0]="http://i67.tinypic.com/smr88i.png";
	         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
	         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
	         		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");  
 				   return;		
 				 }
 				else {
					 System.out.print("565.................");
 					sendText(userId,url,"Please enter valid mobile number.");
 					return;
 				} 				
             } 	 
 			
 			else if((isCity&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))))	
	  			{	  	     
 		       	System.out.println("754.......");
  	             LatitudeAndLongitudeWithPincode lal=new LatitudeAndLongitudeWithPincode();
  	             String latlon[]=lal.getLatLongPositions(text);
				   System.out.println(latlon[0]);
				   System.out.println(latlon[1]);
				     
  						if(!(latlon[0].equalsIgnoreCase("error"))&&!(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end")))
  						{
  						latCust=latlon[0];
  						longiCust=latlon[1];
  						isCity=false;	
						isParagraph=true;
  						cityCust=text;
  						DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
  								{"City","Latitude","Longitude"}, Arrays.asList(text,latCust,longiCust), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
  					   sendText(userId,url,"Tell us something about yourself like your agency name, address,pincode and pan card no");
  	 	        	    return;
  						}
  						else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))
 	  					 {
 	  			    		 System.out.print("507.................");
 	  			    		 DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
 									{"BussinessType","userType"}, Arrays.asList("B2C","3"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
 			    		     DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 	  	 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
 	  			    		String title[]=new String[3];	
 	  	         		    title[0]="New Booking";
 	  	         		    title[1]="Existing Booking";
 	  	         		    title[2]="Agent Connect";
 	  	         		    String urls[]=new String[3];	
 	  	         		    urls[0]="http://i67.tinypic.com/smr88i.png";
 	  	         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
 	  	         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
 	  	         		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");    
 	  					    System.out.println("521.................");
 	  					    return;
 	  					 }
  	           	       					 
  						else{
  						System.out.println("525.......");
  						isCity=false;
  						sendText(userId,url,"Please enter valid city.");
  						return;
  					}
 					  			}
	  					
	  			
	  			
  				else if((isEmailaddressAgent&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))))	
  	  			{
  	  	           	System.out.println("640.......");
  	  				if(isValidEmailAddress(text))
  	  					{
  	  				       System.out.println("642.........");
  	  				        email1=text;
  	  						isEmailaddressAgent=false;
  	  					    isCity=true;
  	  						DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
  	  								{"Emailid"}, Arrays.asList(text), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
  	  					    sendText(userId,url,"What is your current city?");
  	  					    System.out.println(isCity);
  					    	return;
  	  					}
  	  					 else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))
  	  					 {
  	  			    		 System.out.print("416.................");
  	  			    	    DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
									{"BussinessType","userType"}, Arrays.asList("B2C","3"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
			    		 	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
  	  	 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
  	  			    		String title[]=new String[3];	
  	  	         		    title[0]="New Booking";
  	  	         		    title[1]="Existing Booking";
  	  	         		    title[2]="Agent Connect";
  	  	         		    String urls[]=new String[3];	
  	  	         		    urls[0]="http://i67.tinypic.com/smr88i.png";
  	  	         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
  	  	         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
  	  	         		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");    
  	  					    System.out.println("425.................");
  	  					    return;
  	  					 }
  	  					else{
  	  						System.out.println("423.......");
  	  						sendText(userId,url,"Please enter valid email address.");
  	  						return;
  	  					}
  	  			}
  	  			
  				
		    else if((isParagraph&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))))	
  	  			{
  	  						isParagraph=false;
  	  					  DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
  								{"userType"}, Arrays.asList("6"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
  	 					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
  	 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
  	 									+ "fb" + "'"); 				
  	  					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
  	  								{"Paragraph"}, Arrays.asList(text), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
  	  					 paragraph1=text;
  	  					 userDetails= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId).replace("__channelType","fb"));
						 registerTravelAgent(userDetails.get(0)[3],userDetails.get(0)[11],userDetails.get(0)[13],userDetails.get(0)[16],userId,userDetails.get(0)[7],paragraph1,userDetails.get(0)[18],userDetails.get(0)[19]);
						 System.out.println(userDetails.get(0)[3]+"\n"+userDetails.get(0)[11]+"\n"+userDetails.get(0)[13]+"\n"+userDetails.get(0)[16]+"\n"+userDetails.get(0)[7]+"\n"+userDetails.get(0)[18]+"\n"+userDetails.get(0)[19]+paragraph1+userId);
	  					 return;
  	  			}
  	  			
		 if((text!=null)&&text.equalsIgnoreCase("New Booking"))
 			  {
			       System.out.println("425.............");
			       DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{ "BussinessType"}, Arrays.asList("B2C"), "channelId='" + userId + "' and channelType='"
									+ "fb" + "'"); 				
				  DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'"); 				
 					String title[]=new String[3];	
 				    title[0]="Flights";
 				    title[1]="Hotels";
 				    title[2]="Holidays";
 				   String urls[]=new String[3];	
	          		    urls[0]="http://i67.tinypic.com/smr88i.png";
	          		    urls[1]="http://i67.tinypic.com/10z1kwj.png ";
	          		    urls[2]="http://i68.tinypic.com/i23twk.png ";
	     		   sendQuickReplyWithImage(userId,url,"\nWhat would you like to book today?",title,urls,"product type");
 	        	    return;
 		          
 			  } 			  
 			 if((text!=null)&&text.equalsIgnoreCase("Agent Connect"))
 			  {
 				 System.out.println("631............."); 
 				  DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'");
 				  DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{"BussinessType"}, Arrays.asList("B2B2C"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
				  String urls[]=new String[4];	
				  urls[0]="http://i67.tinypic.com/11kvazb.png";
				  urls[1]="http://i65.tinypic.com/iggups.jpg";
				  String title[]=new String[2];	
 				     title[0]="Register";
 				     title[1]="Connect";
 				     System.out.println("\n641.....");
 					 sendQuickReplyWithImage(userId,url,Constants.agentConnectSubHeading,title,urls,"product type eb");
 			         return;		  
 			  } 
 			  if((text!=null)&&text.equalsIgnoreCase("Register"))
 			  {
 				 isMobileNoAgent=true;
	 				sendText(userId,url,"What is your mobile number?");    
	 				System.out.println("649............."); 
	 				DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
	 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
	 									+ "fb" + "'");
					
 		return;
			}
	 if((text!=null)&&text.equalsIgnoreCase("Connect"))
			  {
 				DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
						{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
								+ "fb" + "'");
				 sendText(userId,url," Which city agent would you like to connect?");
				 isCityCustomer=true;
	             return;		  
			  }
 			 
  			 if((isCityCustomer&&(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))))	
 	  			{
 	  	           	System.out.println("754.......");
 	  	             LatitudeAndLongitudeWithPincode lal=new LatitudeAndLongitudeWithPincode();
 	  	             String latlon[]=lal.getLatLongPositions(text);
					   System.out.println(latlon[0]);
					   System.out.println(latlon[1]);
					     
 	  						if(!(latlon[0].equalsIgnoreCase("error"))&&!(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end")))
 	  						{
 	  						latCust=latlon[0];
 	  						longiCust=latlon[1];
 	  						isCityCustomer=false;
 	  						cityCust=text;
 	  						DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
 	  								{"City","Latitude","Longitude"}, Arrays.asList(text,latCust,longiCust), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
 	  						DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
	  	 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("waitingForMatch","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
 	  						String title[]=new String[3];	
 	  	 				    title[0]="Flights";
 	  	 				    title[1]="Hotels";
 	  	 				    title[2]="Holidays";
 	  	          		    String urls[]=new String[3];	
 	  	          		    urls[0]="http://i67.tinypic.com/smr88i.png";
 	  	          		    urls[1]="http://i67.tinypic.com/10z1kwj.png ";
 	  	          		    urls[2]="http://i68.tinypic.com/i23twk.png ";
 	  	     				sendQuickReplyWithImage(userId,url,"\nWhat would you like to enquire about?",title,urls,"product type");
 	  	 	        	    return;
 	  						}
 	  						else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))
 	 	  					 {
 	 	  			    		 System.out.print("507.................");
 	 	  			    	   DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
 									{"BussinessType","userType"}, Arrays.asList("B2C","3"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
 			    		 	   DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 	 	  	 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
 	 	  			    		String title[]=new String[3];	
 	 	  	         		    title[0]="New Booking";
 	 	  	         		    title[1]="Existing Booking";
 	 	  	         		    title[2]="Agent Connect";
 	 	  	         		    String urls[]=new String[3];	
 	 	  	         		    urls[0]="http://i67.tinypic.com/smr88i.png";
 	 	  	         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
 	 	  	         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
 	 	  	         		    sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");    
 	 	  					    System.out.println("521.................");
 	 	  					    return;
 	 	  					 }
 	  	           	       					 
 	  						else{
 	  						System.out.println("525.......");
 	  						isCity=false;
 	  						sendText(userId,url,"Please enter valid city.");
 	  						return;
 	  					}
 	  			}
 			 if((text!=null)&&text.equalsIgnoreCase("Existing Booking"))
			  {
				 System.out.println("670............."); 
				 DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{ "BussinessType"}, Arrays.asList("B2C"), "channelId='" + userId + "' and channelType='"
									+ "fb" + "'");
				  DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
									+ "fb" + "'");
				  String urls[]=new String[4];	
				  urls[0]="http://i63.tinypic.com/25t8c1w.jpg";
				  urls[1]="http://i64.tinypic.com/20q11me.png";
				  urls[2]="http://i64.tinypic.com/72awih.png";		
				  urls[3]="http://i68.tinypic.com/1z3wi11.png";	
	            	 String title[]=new String[4];	
				     title[0]="Cancellation";
				     title[1]="Reschedule";
				     title[2]="Refund";
				     title[3]="Feedback/ Suggestion";
					 System.out.println("\n684....");
					 sendQuickReplyWithImage(userId,url,Constants.existingBookingHeading,title,urls,"product type eb");
			         return;		  
			  } 			  
 			  if((text!=null)&&(text.equalsIgnoreCase("Cancellation")))
 			  {
 				    System.out.println("479.............");
 				   DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{"ProductId"}, Arrays.asList("5"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
                 	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'");
 					sendText(userId,url,"Please Enter your EaseMyTrip Confirmation Number.");	 					
 					isConfirmationNo=true;
 	            	return;		  	
 			  }
 			  
 			  if((text!=null)&&text.equalsIgnoreCase("Reschedule"))
 			  {
 				    System.out.println("491.............");
 					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'");
 					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{"ProductId"}, Arrays.asList("6"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
                	sendText(userId,url,"Please Enter your EaseMyTrip Confirmation Number.");	
 					isConfirmationNo=true;
 	            	return;		  	
 			  }


 			  if((text!=null)&&text.equalsIgnoreCase("Refund"))
 			  {
 				    System.out.println("504.............");
 					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'");
 					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{"ProductId"}, Arrays.asList("4"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
                	sendText(userId,url,"Please Enter your EaseMyTrip Confirmation Number.");	
 					isConfirmationNo=true;
 	            	return;		  	
 			  }
 			  
 			 if((text!=null)&&text.equalsIgnoreCase("Feedback/ Suggestion"))
			  {
				    System.out.println("516.............");
					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
							{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
									+ "fb" + "'");
					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
							{"ProductId"}, Arrays.asList("7"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");   	 
                	sendText(userId,url,"Please Enter your EaseMyTrip Confirmation Number.");	
					isConfirmationNo=true;
	            	return;		  	
			  }
 			 
 			  if(isConfirmationNo)
 			 {
 				System.out.println("479............."); 
 				String mobileNo=" ",queryNo=" ",bussinessType=" ",message=" ";
 				email=" ";
 				name=" ";
 				boolean status=false;				
 				String s=getExistingDetails(text,"1"); 
				s=XML.toJSONObject(s).toString();
				System.out.println("507............"+s);
				try{
				Gson gson = new GsonBuilder().setPrettyPrinting().create();
				Example_APIResponse exRes = gson.fromJson(s,Example_APIResponse.class);	
				status=exRes.getAPIResponse().getStatus();
				message=exRes.getAPIResponse().getMessage();
				if(status){
					isConfirmationNo=false;
					queryNo=exRes.getAPIResponse().getQueryNumber();
					name=exRes.getAPIResponse().getName();
					bussinessType=exRes.getAPIResponse().getCustomerType();
					email=exRes.getAPIResponse().getEmailId();
					mobileNo=exRes.getAPIResponse().getMobileNo().toString();	
					System.out.println(queryNo+"\n"+bussinessType+"\n"+name+"\n"+email+"\n"+mobileNo+"\n"+status+"\n"+message+"\n");
					}
				else if(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end")){
 					isMobileNo=false;
 					 System.out.print("267.................");
 		    		DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
 								{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"+"fb"+ "'");
 		    		String title[]=new String[3];	
         		    title[0]="New Booking";
         		    title[1]="Existing Booking";
         		    title[2]="Agent Connect";
         		    String urls[]=new String[3];	
         		    urls[0]="http://i67.tinypic.com/smr88i.png";
         		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
         		    urls[2]="http://i65.tinypic.com/w9ziup.png";
         		    sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
				    System.out.println("220.................");
 				    
 				   return;		
 				 }
				else{
					sendText(userId,url,"Please enter valid Confirmation Number.");
				}
				
				}
				catch(Exception e)
				{
					e.printStackTrace();
					logExceptionInDB(userId,"fb",text,e);
				}
				if(status){
					DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]{ "BussinessType", "Mobile","IsVerifiedMobile","EmailId","userType"},Arrays.asList(bussinessType,mobileNo,"1",email,"5"), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'");
					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{ "State", "CurrentPreference", "UpdateTime" },Arrays.asList("waitingForMatch","chat",dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
 									+ "fb" + "'");	
				 }
				System.out.println("1268.................");
 				chattingMethod(test1,test,response,FB_MSG_URL,profileLink);
 				return; 			
 			 }
	 
        		if(((!text.isEmpty()&&!isMobileNo&&!isEmailaddress&&!isEmailOrMobile&&!isEmailaddressCustomer&&!isEmailadd&&!isConfirmationNo&&!isCity&&!isParagraph&&!isWaitingMessageSent)||text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end"))&&userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
        		{
        			   System.out.println("935............."+!isParagraph+!isEmailaddressCustomer);
        			   userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId));
   				       System.out.println("376............"+userDetails.get(0)[14]); 
   				 if(pageId.equalsIgnoreCase("1156653614453520"))
                     {
                     if(text.equalsIgnoreCase("GetChannelId"))
     				 {
                     	 sendText(userId,url,"Your channelID is"+userId);
     				 }
                     if(userDetails.get(0)[15].equalsIgnoreCase("6"))
                     {
                    	 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                    	 sendText(userId,url,"Welcome to EaseMyTrip.");
           		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId
     							+ "' and channelType='" + "fb" + "'");
        				 return; 
                     }            	 
                     else{	 
                    	sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                     	String title[]=new String[3];	
              		    title[0]="New Booking";
              		    title[1]="Existing Booking";
              		    title[2]="Agent Connect";
              		    String urls[]=new String[3];	
              		    urls[0]="http://i65.tinypic.com/4zwahe.png";
              		    urls[1]="http://i66.tinypic.com/35b9sfl.png";
              		    urls[2]="http://i65.tinypic.com/w9ziup.png";
              		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
              		   DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("welcomeMessageSent", "none",dateFormat.format(new Date())), "channelId='" + userId
  							+ "' and channelType='" + "fb" + "'");
              		   return;
                  	     }  
                     }
                     else{
                    	 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                     	 sendText(userId,url,"Welcome to EaseMyTrip.\n");
                         }

        			if((userDetails.size()>0)&&(userDetails.get(0)[15].equalsIgnoreCase("3"))){
							 
							 if (userStateReportData.size() > 0)
								{
			
								 DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
											{ "State", "CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
													+ "fb" + "'");
									}
							 return;
        			  }
 			      System.out.println("376.................");
 			       return;
 				 }  
        		if((payload!=null&&payload.contains("startchattingevent"))||(text.equalsIgnoreCase("menu")||text.equalsIgnoreCase("end")||!text.isEmpty())&&(userStateReportData.size()==0||((userStateReportData.size()>0)&&(!(userStateReportData.get(0)[3].equalsIgnoreCase("matched"))&&(!(userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch")))))))
          		{        			
        			    //System.out.println("412................"+text+userStateReportData.get(0)[3]);        			   
                        userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId));
                        System.out.println("579................."+text);
                        users=DataBaseHandler.getInstance().readData(Constants.usersQuery.replace("__channelId",userId));
                        System.out.println("622................."+pageId);
                        int i,userType=5;
                    	String productId=" ";
                    	if(users.size()>0){
                    		userType=3;                    	
                        	for(i=0;i<users.size();i++){
                        		System.out.println(users.get(i)[22]);
                        		if(i==users.size()-1)
                        		productId+=users.get(i)[22];
                        		else
                            	productId+=users.get(i)[22]+",";
                        	}
                        }
                        if(pageId.equalsIgnoreCase("1156653614453520"))
                        {
                        	System.out.println("639.............");
                        if(text.equalsIgnoreCase("GetChannelId"))
        				 {
                        	 sendText(userId,url,"Your channelID is"+userId);
        				 }
                        
                        if((userDetails.size()>0)&&(userDetails.get(0)[15].equalsIgnoreCase("6")))
                        {
                         System.out.println("1050");
                       	 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                       	 sendText(userId,url,"Welcome to EaseMyTrip.");
           		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId
     							+ "' and channelType='" + "fb" + "'");
           				 return; 
                        }            	 
                        else{	 
                        	System.out.println("1058");
                        	sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                        	String title[]=new String[3];	
                  		    title[0]="New Booking";
                  		    title[1]="Existing Booking";
                  		    title[2]="Agent Connect";
                  		    String urls[]=new String[3];	
                  		    urls[0]="http://i65.tinypic.com/4zwahe.png";
                  		    urls[1]="http://i66.tinypic.com/35b9sfl.png";
                  		    urls[2]="http://i65.tinypic.com/w9ziup.png";
                  		   sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
                        	}  
                        }
                        else{
                        	 sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
                        	 sendText(userId,url,"Welcome to EaseMyTrip.\n");
                            }
        			
        		    firstName=profile.getFirstName();
        			lastName=profile.getLastName();
        			profilePic=profile.getProfilePic();
        			locale=profile.getLocale();
        			gender=profile.getGender();
        			timeZone=profile.getTimezone();
        			dob1="01/01/1994";
        			System.out.println("Dob:"+dob1);
        			name=firstName+lastName;
        			System.out.println("FirstName:"+firstName);
        			System.out.println("LastName:"+lastName);
        			System.out.println("Profile Pic:"+profilePic);
        			System.out.println("Locale:"+locale);
        			System.out.println("Gender:"+gender);
        			System.out.println("userId"+userId);
        			System.out.println("timeZone"+timeZone);
        			if(timeZone.equalsIgnoreCase("5.5"))
        			{
        				country="India";
        			}
        			else if(timeZone.equalsIgnoreCase("8"))
        			{
        				country="Phillipines";
        			}
        			else if(timeZone.equalsIgnoreCase("6.5"))
        			{
        				country="Myanmar";
        			}
        			
        			else if(timeZone.equalsIgnoreCase("5"))
        			{
        				country="Pakistan";
        			}
        			else if(timeZone.equalsIgnoreCase("5.75"))
        			{
        				country="Nepal";
        			}

        			String[] userDetailsCols = Constants.userDetailsCols.split(",");
        			String[] args = {};
        		    System.out.println(userDetails.size());
        			if (userDetails.size() == 0)
        			{       				
        				userDataValues.add(userId);
        				userDataValues.add("fb");
        				userDataValues.add(firstName+" "+lastName);
        				userDataValues.add(profilePic);
        				userDataValues.add(gender);
        				userDataValues.add("0");
        				userDataValues.add(country);
        				userDataValues.add(" ");
        				userDataValues.add("B2C");
        				userDataValues.add(pageId);
        				userDataValues.add("0");
        				userDataValues.add("0");
        				userDataValues.add(" ");
        				userDataValues.add("5");
        				userDataValues.add(" ");
        				userDataValues.add(" ");
        				System.out.println(userDetails.size());
        				DataBaseHandler.getInstance().addData(Constants.userDetailsTable, userDetailsCols, userDataValues);
        				userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId)
        							.replace("__channelType","fb"));	
        					}
        			if((users.size() > 0)&&(users.get(0)[8].equalsIgnoreCase("1")))
        			{
        				System.out.println("719.......");
        				// Update
        				DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
        						{"ProductId","userType"}, Arrays.asList(productId,Integer.toString(userType)), "channelId='" + userId + "' and channelType='"
        								+ "fb" + "'");
        				if (userStateReportData.size() > 0)
            			{
            				System.out.println("1143");
            				// Update
            				DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
            						{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("waitingForMatch","chat", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
            								+ "fb" + "'");
            			    return;	
                			
            				}
            			else
            			{
            				List<String> userStateDataValues=new ArrayList();
            				userStateDataValues.add(userId);
            				userStateDataValues.add("fb");
            				userStateDataValues.add("waitingForMatch");
            				userStateDataValues.add("chat");
            				userStateDataValues.add(dateFormat.format(new Date()));
            				userStateDataValues.add("0");
            	            String userStateColumns[]=Constants.userStateCols.split(",");
            				System.out.println("217");
            				userStateDataValues = userStateDataValues.subList(0,userStateColumns.length);
            				DataBaseHandler.getInstance().addData(Constants.userStateTable, userStateColumns, userStateDataValues);
            			    return;	
                			
            			}
        			}
        			if (userStateReportData.size() > 0)
        			{
        				System.out.println("405");
        				// Update
        				DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
        						{ "State", "CurrentPreference", "UpdateTime" }, Arrays.asList("welcomeMessageSent", "none", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
        								+ "fb" + "'");
        			}
        			else
        			{
        				List<String> userStateDataValues=new ArrayList();
        				userStateDataValues.add(userId);
        				userStateDataValues.add("fb");
        				userStateDataValues.add("welcomeMessageSent");
        				userStateDataValues.add("none");
        				userStateDataValues.add(dateFormat.format(new Date()));
        				userStateDataValues.add("0");
        	            String userStateColumns[]=Constants.userStateCols.split(",");
        				System.out.println("217");
        				userStateDataValues = userStateDataValues.subList(0,userStateColumns.length);
        				DataBaseHandler.getInstance().addData(Constants.userStateTable, userStateColumns, userStateDataValues);
        			}
        			return;
        	  }	
  }
	
   private Distance nearByAgent(String channelId)
   {
        System.out.println(channelId);
	    List<AgentDet> agentDetail = new ArrayList<>();
		String email,product,city,mobileNo,country,agentName,paragraph;
		Double latitude,longitude;
		Long chanlId;
		int count=0;
		String detailAgent=getAgentDetails();
		    try {
				detailAgent=XML.toJSONObject(detailAgent).toString();
				System.out.println(detailAgent);
				JSONObject jsonObj = new JSONObject(detailAgent);
				JSONObject c = jsonObj.getJSONObject("diffgr:diffgram");
				JSONObject dataSet = c.getJSONObject("NewDataSet");
				JSONArray ja_data = dataSet.getJSONArray("Table");
				int length = ja_data.length();
				System.out.println(length);
				for(int i=0; i<length; i++) {
				  JSONObject jsonObject= ja_data.getJSONObject(i);	  
				  email=jsonObject.getString("Email");
				  paragraph=jsonObject.getString("AgentDetails");
				  product=jsonObject.getString("Product");
				  longitude=jsonObject.getDouble("Longitude");
				  latitude=jsonObject.getDouble("Latitude");
				  chanlId=jsonObject.getLong("ChannelId");
				  city=jsonObject.getString("City");
				  mobileNo=jsonObject.getString("MobileNumber");
				  country=jsonObject.getString("Country");
				  agentName=jsonObject.getString("AgentName");
				  //System.out.println(email+'\n'+paragraph+'\n'+product+'\n'+longitude+'\n'+latitude+'\n'+channelId+'\n'+city+'\n'+mobileNo+'\n'+country+'\n'+agentName);
				 agentDetail.add(new AgentDet(email,paragraph,product,latitude,longitude,chanlId,city,mobileNo,country,agentName));
				}
				for (int i = 0; i < agentDetail.size(); i++) {
			             System.out.println(agentDetail.get(i).getEmail());
			        }
			}
		catch (JSONException e1) 
		    {
			    logExceptionInDB(userId,"fb","nearByAgent",e1);
				e1.printStackTrace();
		    }
	    city=" ";
     	int dept=0;
	    int i;
	    double dist=0.0;
	    List<String[]> prod=new ArrayList<>();
	    Distance d1 = null;
	    try { 
		List<String[]> userDetails= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",channelId));
		if(!userDetails.get(0)[8].equalsIgnoreCase(" "))
		{
		dept=Integer.parseInt(userDetails.get(0)[8]);
		prod= DataBaseHandler.getInstance().readData(Constants.products.replace("_id",String.valueOf(dept)));
		
		ArrayList<Distance> distList=new ArrayList<Distance>();
		for(i=0;i<agentDetail.size();i++)
	    {
			System.out.println("1467"+prod.get(0)[0]);
			System.out.println("1468"+"\n"+agentDetail.get(0).getProduct());
			System.out.println("1469"+"\n"+agentDetail.get(1).getProduct());
			System.out.println("1470"+"\n"+agentDetail.get(2).getProduct());
			System.out.println("1558"+prod.get(0)[0]+"\n"+agentDetail.get(i).getProduct()+"\n"+agentDetail.get(i).getLatitude()+"\n"+agentDetail.get(i).getLongitude());
		if(agentDetail.get(i).getProduct().toLowerCase().contains(prod.get(0)[0].toLowerCase()))
		{
		System.out.println("1561............if condition satisfied.");	
		System.out.println(Double.parseDouble(userDetails.get(0)[18])+" "+Double.parseDouble(userDetails.get(0)[19])+agentDetail.get(i).getLatitude()+agentDetail.get(i).getLongitude());
	    dist=DistanceCalculator.distance(Double.parseDouble(userDetails.get(0)[18]),Double.parseDouble(userDetails.get(0)[19]),agentDetail.get(i).getLatitude(),agentDetail.get(i).getLongitude());
		}
	    System.out.println("Dist 1460..."+dist);		
		distList.add(new Distance(userDetails.get(0)[1],String.valueOf(agentDetail.get(i).getChannelId()),dist));       	    
	    System.out.println("1463"+String.valueOf(agentDetail.get(i).getChannelId()));		
	    }
	    for (Distance distan :distList) {
	        System.out.println(distan.getChannelId1()+ "   "+ distan.getChannelId2()+" "+distan.getDist());
	    }
	    Collections.sort(distList,new DistanceComparator());
	    System.out.println("After sorting");
	    for (Distance distan : distList) {
	    	
	    	 List<String[]> userstate= DataBaseHandler.getInstance().readData(Constants.userStateQuery.replace("__channelId",distan.getChannelId2()));
	         if(userstate.get(0)[3].equalsIgnoreCase("waitingForMatch"))
	         {
	         d1=new Distance(distan.getChannelId1(),distan.getChannelId2(),distan.getDist());	 
	    	 System.out.println("1474"+distan.getChannelId1()+ "   "+ distan.getChannelId2()+" "+distan.getDist());
	    	 break;
	         }
	        if(userstate.get(0)[3].equalsIgnoreCase("matched")||userstate.get(0)[3].equalsIgnoreCase("welcomeMessageSent"))
	         {
	          d1=new Distance();
	         System.out.println("1588"+distan.getChannelId1()+ "   "+ distan.getChannelId2()+" "+distan.getDist());
	    	 continue;
	         }
	    }
	    }
	    }catch (Exception e) 
	   {
	    	logExceptionInDB(userId,"fb","nearByAgent",e);		
		  e.printStackTrace();
	    }		
	   return d1;
	 }
   class Distance{  
	    String channelId1;  
	    String channelId2;  
	    double dist;  
	    public Distance()
	    {
	    	channelId1="1111111";
	    	channelId2="1111111";
	    	dist=0.0;
	    }
	    Distance(String channelId1,String channelId2,double dist)
	    {
	        this.channelId1=channelId1;  
	        this.channelId2=channelId2;  
	        this.dist=dist;  
	    }
	    public String getChannelId1() {
	        return channelId1;
	    }

	    public void setChannelId1(String channelId1) {
	        this.channelId1 = channelId1;
	    }

	    public String getChannelId2() {
	        return channelId2;
	    }

	    public void setChannelId2(String channelId2) {
	        this.channelId2 = channelId2;
	    }
	    
	    public Double getDist() {
	        return dist;
	    }

	    public void setDist(Double dist) {
	        this.dist = dist;
	    }
   }
   
   public class DistanceComparator implements Comparator<Distance> {

	@Override
	public int compare(Distance o1, Distance o2) {
		return (int) (o1.getDist() - o2.getDist());
		// TODO Auto-generated method stub
		//return 0;
	}
   }
private void chattingMethod(RequestModel model,String test,HttpServletResponse response, String fB_MSG_URL,String profileLink2) 
	{
	
	    String city;
	    System.out.println("527............");
		String text=" ",url;
		String pageId=model.getEntry().get(0).getId();
		List<String> userDataValues=new ArrayList();
		List<String[]> findMatchData = new ArrayList<String[]>();
		url=fB_MSG_URL+map.get(pageId);
		Distance d=null;
		String userId=model.getEntry().get(0).getMessaging().get(0).getSender().getId();
		response.setStatus(HttpServletResponse.SC_OK);
		try
		{
			System.out.println("1131............");
			boolean isVisited=false;
			String encode=" ";
			String userStateQuery = Constants.userStateQuery.replace("__channelId",userId);
			List<String[]> userStateReportData = DataBaseHandler.getInstance().readData(Constants.userStateQuery.replace("__channelId",userId));
			List<String[]> userDetail1 = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId));
			System.out.println("1137............");
			if((userStateReportData.size()>0)&&(userStateReportData.get(0)[3].equalsIgnoreCase("matched")&&(userStateReportData.get(0)[4].equalsIgnoreCase("chat")||userStateReportData.get(0)[4].equalsIgnoreCase("end"))))
			{
			if(((model.getEntry().get(0).getMessaging().get(0).getMessage())!=null)&&((model.getEntry().get(0).getMessaging().get(0).getMessage().getText())!=null)) 
			  {
				    System.out.println("1165");
					text=model.getEntry().get(0).getMessaging().get(0).getMessage().getText();
					System.out.println("text162..."+text);
					text=text.trim();
				    }
				userId=model.getEntry().get(0).getMessaging().get(0).getSender().getId();
				List<String[]> matchedUser = DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId",userId));
				System.out.println(matchedUser.get(0)[3]+"1150");
				List<String[]> userDetail =DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",matchedUser.get(0)[3]));
				if(userDetail.get(0)[9].equalsIgnoreCase("B2B2C"))
				{
				 d=nearByAgent(userId);
				}
				System.out.println("  "+"userDetail.get(0)[9]"+text);
				fB_MSG_URL=fB_MSG_URL+map.get(userDetail.get(0)[10]);
				 if(text.matches("[1-9][0-9]{9,14}"))
					{						
						return;
					}
				 
				 else if(text.matches("\\b(https?|ftp|file)://[-a-zA-Z0-9+&@#/%?=~_|!:,.;]*[-a-zA-Z0-9+&@#/%=~_|]")||text.contains(".com")||text.contains(".co.in")||text.contains(".in"))
				 {
					 if(text.contains("easemytrip")){
					    System.out.println("1389");
					 }
					 else{
						 System.out.println("1394");
						 return; 
					 }
				 }
				 if((text.equalsIgnoreCase("end") || text.equalsIgnoreCase("exit")||text.equalsIgnoreCase("menu")))
				{
					isCityCustomer=false; 
				    System.out.println("1152............");
					isVisited=true;
					System.out.println("Last convo ended 501");
					DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
							{ "CurrentPreference" }, Arrays.asList("end"), "channelId='" + userId + "' and channelType='" + "fb" + "'");
					String options[]=new String[2];
					options[0]="YES";
					options[1]="NO";
					sendQuickReply(userId,url,"Are you sure want to end this conversation?",options,"end");		
					return;
				}
				   if((text.equalsIgnoreCase("yes")||text.equalsIgnoreCase("no"))&&(userStateReportData.get(0)[4].equalsIgnoreCase("end")))
					{
					    isCityCustomer=false;
						System.out.println("1165");
						isVisited=false;
						if(text.equalsIgnoreCase("no"))
						{
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{ "CurrentPreference" }, Arrays.asList("chat"), "channelId='" + userId + "' and channelType='" + "fb" + "'");
						return;
						}
				     	System.out.println("505");
						matchedUser = DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId",userId));
						System.out.println("558.................");
						System.out.println(userId+"Title"+url);
						List<String[]> userDetails1= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",matchedUser.get(0)[3]).replace("__channelType","fb"));
						List<String[]> userDetails= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId).replace("__channelType","fb"));
						if(userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
						{
							System.out.println("1184");	                     	
							String title[]=new String[3];	
	              		    title[0]="New Booking";
	              		    title[1]="Existing Booking";
	              		    title[2]="Agent Connect";
	              		    String urls[]=new String[3];	
	              		    urls[0]="http://i65.tinypic.com/4zwahe.png";
	              		    urls[1]="http://i66.tinypic.com/35b9sfl.png";
	              		    urls[2]="http://i65.tinypic.com/w9ziup.png";
	              		System.out.println("User Details 1521"+userDetails.get(0)[15]);   
	              		System.out.println("User Details 1522"+userDetails1.get(0)[15]);    
	              		if(userDetails.get(0)[15].equalsIgnoreCase("6"))
	              		{
	              		    	System.out.println("1192");
	              		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" + matchedUser.get(0)[3]
	        							+ "' and channelType='" + "fb" + "'");
	    				        DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId
	    						+ "' and channelType='" + "fb" + "'");	
                                sendText(matchedUser.get(0)[3],url,"Last conversation ended.");	
								sendImage(matchedUser.get(0)[3],url,"http://i66.tinypic.com/2pql3qp.png");
	    				        sendQuickReplyWithImage(matchedUser.get(0)[3],url,Constants.welcomeMsg,title,urls,"menu");
		              		    sendText(userId,url,"Last conversation ended.");
		              		    sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
	    				        sendText(userId,url,"Welcome to EaseMyTrip.");	
		              		    return;
	              		}  
	              		else if(userDetails1.get(0)[15].equalsIgnoreCase("6"))
	              		{
	              		    	System.out.println("1244");
	              		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + matchedUser.get(0)[3]
	        							+ "' and channelType='" + "fb" + "'");
	              		    	DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("welcomeMessageSent","none", dateFormat.format(new Date())), "channelId='" + userId
	     	    						+ "' and channelType='" + "fb" + "'");	
	              		       	sendText(userId,url,"Last conversation ended.");	
							    sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
								sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
			              		sendText(matchedUser.get(0)[3],url,"Last conversation ended.");	
							    sendImage(matchedUser.get(0)[3],url,"http://i66.tinypic.com/2pql3qp.png"); 
							    sendText(matchedUser.get(0)[3],url,"Welcome to EaseMyTrip.");	
	              		        return;
	              		    }
	              		    }
						else if(userDetails.get(0)[9].equalsIgnoreCase("B2C"))
						{
						System.out.println("1209............");
				     	String title[]=new String[3];	
              		    title[0]="New Booking";
              		    title[1]="Existing Booking";
              		    title[2]="Agent Connect";
              		    String urls[]=new String[3];	
              		    urls[0]="http://i65.tinypic.com/4zwahe.png";
              		    urls[1]="http://i66.tinypic.com/35b9sfl.png";
              		    urls[2]="http://i65.tinypic.com/w9ziup.png";
              		   if(pageId.equalsIgnoreCase("1156653614453520")){
						DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + matchedUser.get(0)[3]
							+ "' and channelType='" + "fb" + "'");

					  //  Distance d=nearByAgent(userId);
					            DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("welcomeMessageSent","none", dateFormat.format(new Date())), "channelId='" + userId
						+ "' and channelType='" + "fb" + "'");	
				        sendText(userId,url,"Last conversation ended.");	
						sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");
	                    sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
	                    url="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetails1.get(0)[10]);
	                	sendText(matchedUser.get(0)[3],url,"Last conversation ended.");	
	                    sendImage(matchedUser.get(0)[3],url,"http://i66.tinypic.com/2pql3qp.png"); 
	                    sendText(matchedUser.get(0)[3],url,"Welcome to EaseMyTrip.");	       				       
	                    return;     
              		   }
	                        else
	                        {
	                    	    System.out.println("1232............");
	                    		sendText(matchedUser.get(0)[3],url,"Last conversation ended.");	
	                    		sendText(userId,url,"Last conversation ended.");
	                 		    sendImage(userId,url,"http://i66.tinypic.com/2pql3qp.png");  	                
	                        	sendText(userId,url,"Welcome to EaseMyTrip.\n");
	                 		    url="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetails1.get(0)[10]);
	                 		    sendText(matchedUser.get(0)[3],url,"Last conversation ended.");
	                 		    sendImage(matchedUser.get(0)[3],url,"http://i66.tinypic.com/2pql3qp.png");
	                    		sendQuickReplyWithImage(matchedUser.get(0)[3],url,Constants.welcomeMsg,title,urls,"menu");
	                    		DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("welcomeMessageSent","none", dateFormat.format(new Date())), "channelId='" + matchedUser.get(0)[3]
										+ "' and channelType='" + "fb" + "'");	
						        DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]{"State","CurrentPreference","UpdateTime"}, Arrays.asList("waitingForMatch", "chat", dateFormat.format(new Date())), "channelId='" + userId
								+ "' and channelType='" + "fb" + "'");
						        return;
	                            }
                    
						}
						return;
					
					}
					if((model.getEntry().get(0).getMessaging().get(0).getMessage())!=null&&(model.getEntry().get(0).getMessaging().get(0).getMessage().getText())!=null){
					    System.out.println("1249............");
						text=model.getEntry().get(0).getMessaging().get(0).getMessage().getText();
						System.out.println("text");
						text=text.trim();
						List<String[]> userDetail2=DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",matchedUser.get(0)[1]));
						System.out.println("CITY is........"+url+"Text......."+text);
						//fB_MSG_URL=fB_MSG_URL+map.get(userDetail.get(0)[10]);
						fB_MSG_URL="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetail.get(0)[10]);
						//System.out.println("CITY is........"+fB_MSG_URL+"Text......."+text);
						sendText(matchedUser.get(0)[3],fB_MSG_URL,text);
					    encode=StringEscapeUtils.escapeJava(text);
				   }
					else if((model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment()!=null)&&(model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getType()).equalsIgnoreCase("audio"))
					{
					System.out.println("1263............");	
					System.out.println("Audio");
					encode=model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getPayload().getUrl();
					fB_MSG_URL="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetail.get(0)[10]);			      
					sendAudio(matchedUser.get(0)[3],fB_MSG_URL,encode);
				    }				
					else if((model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment()!=null)&&(model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getType()).equalsIgnoreCase("file"))
					{
					System.out.println("1271............");	
					System.out.println("File");
					encode=model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getPayload().getUrl();
					fB_MSG_URL="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetail.get(0)[10]);			      
					sendFile(matchedUser.get(0)[3],fB_MSG_URL,encode);
				    }
					
					else if((model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment()!=null)&&(model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getType()).equalsIgnoreCase("location"))
					{
					System.out.println("1280............");	
					Double lat,longitude;
					System.out.println("Location");
					System.out.println("testing body  "+test);
					String link;
				    Gson gson = new GsonBuilder().setPrettyPrinting().create();
					ExampleLocation exRes = gson.fromJson(test,ExampleLocation.class);	
					lat=exRes.getEntry().get(0).getMessaging().get(0).getMessage().getAttachments().get(0).getPayload().getCoordinates().getLat();
				    longitude=exRes.getEntry().get(0).getMessaging().get(0).getMessage().getAttachments().get(0).getPayload().getCoordinates().getLong();
				    System.out.println("Latitude"+lat);
					System.out.println("Longitude"+longitude);
					GetReverseGeoCoding rgc=new GetReverseGeoCoding();
					rgc.getAddress(lat,longitude);
					System.out.println("dgffgfg"+rgc.getPIN());
					System.out.println("sdfgfggh"+rgc.getCity());
					System.out.println("dffgghhg"+rgc.getState());
					System.out.println("Address1500.........."+rgc.getAdd());					
					sendText(matchedUser.get(0)[3],fB_MSG_URL,"Client's location is\n"+rgc.getCity()+","+rgc.getCountry()+","+rgc.getPIN());
					}
					else if((model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment()!=null)&&(model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getType()).equalsIgnoreCase("image"))
					{
					System.out.println("1300............");
					System.out.println("Image");
					encode=model.getEntry().get(0).getMessaging().get(0).getMessage().getAttachment().get(0).getPayload().getUrl();
					fB_MSG_URL="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetail.get(0)[10]);
					sendImage(matchedUser.get(0)[3],fB_MSG_URL,encode);
			        }	
					matchedUser= DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId",userId)
								.replace("__channelType","fb"));
					DataBaseHandler.getInstance().updateSessionChatCount(userId,matchedUser.get(0)[3]);
					DataBaseHandler.getInstance().addData(Constants.chatHistoryTable, Constants.chatHistoryCols.split(","), Arrays.asList(userStateReportData.get(0)[6],userId, matchedUser.get(0)[3], encode,dateFormat.format(new Date())));
		} 
		if(userStateReportData.size()>0&&userStateReportData.get(0)[3].equalsIgnoreCase("waitingForMatch")&&userStateReportData.get(0)[4].equalsIgnoreCase("chat"))
			{
			System.out.println("userStateReportData"+userStateReportData.size());
			System.out.println("1274"+userStateReportData.get(0)[3]);
			System.out.println("text"+text+!(text.equalsIgnoreCase("t")));
			System.out.println("1276");
			String finndMatchQuery = "";
			List<String[]> matchedUser = DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId",userId));
			System.out.println("matchedUser218");
			String[] userDetailsCols = Constants.userDetailsCols.split(",");
			List<String[]> userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId)
						.replace("__channelType","fb"));
			System.out.println(matchedUser.size());
		
			if((!text.isEmpty()))
			{
		    	userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId));
				System.out.println("1287..........."+userDetails.get(0)[14]); 
				System.out.println("1290......"+userDetails.get(0)[15]);
				System.out.println("1291....."+userDetails.get(0)[15]);
			if (matchedUser.size() > 0) 
  			{
			System.out.println("1290");
			if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
			{
				flag=false;
				String deptClient=userDetails.get(0)[8].toString();
				finndMatchQuery =Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
				findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
				System.out.println("findMatchData226"+findMatchData);
						}
			if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
			{
				flag=false;
				String deptClient=userDetails.get(0)[8].toString();
			    String cityCust = userDetails.get(0)[16].toString();
			    d=nearByAgent(userId);
				  System.out.println(d.getChannelId2());
			    if(d.getChannelId2().equalsIgnoreCase("1111111"))
			    		{
			    	       System.out.println("1938");
			    		}
			    else
			    {
			    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
					 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
					 System.out.println("findMatchData1305"+findMatchData);
	
			    }
		 }
			DataBaseHandler.getInstance().deleteData("delete from tblFB_MatchUsers where channelId = '" +userId + "'");
			}					
		 else
		 {
			 System.out.println("1312");
			 matchedUser = DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId",userId));
			 userDetailsCols = Constants.userDetailsCols.split(",");
			 userDetails = DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId).replace("__channelType","fb"));
			 finndMatchQuery = "";			
			 System.out.println("userDetails247"+userDetails.get(0)[15]);
			 if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
			 {
					flag=false;
					String deptClient=userDetails.get(0)[8].toString();
					finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
					findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
					
			 }
			 if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
				{
				 d=nearByAgent(userId);
					flag=false;
					  System.out.println(d.getChannelId2());
				    if(d.getChannelId2().equalsIgnoreCase("1111111"))
		    		{
		    	       System.out.println("1968");
		    		}
		    else
		    {
		    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
				 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
				 System.out.println("findMatchData1305"+findMatchData);

		    }
				    }
		           System.out.println("Find Match Data"+findMatchData.size());
				}
				 if (findMatchData.size() == 0)
					{
						String myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						Thread.sleep(1 * 60 * 1000);
						String myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						String myStateQuery = Constants.userStateQuery.replace("__channelId",userId);
						List<String[]> myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						System.out.println("1343");
						if (!myOldChatCount.equalsIgnoreCase(myNewChatCount)||!myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						    return;
						myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						if (myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						{
							System.out.println("1349");
						    DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{ "CurrentPreference", "UpdateTime" }, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
							String deptClient=userDetails.get(0)[8].toString();
							if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
							 {
									flag=false;
									deptClient=userDetails.get(0)[8].toString();
									finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
									findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
									
							 }
							if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
							{
							 d=nearByAgent(userId);
								flag=false;

								  System.out.println(d.getChannelId2());
							    if(d.getChannelId2().equalsIgnoreCase("1111111"))
					    		{
					    	       System.out.println("2010");
					    		}
							    
					    else
					    {
					    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
							 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
							 System.out.println("findMatchData1305"+findMatchData);

					    }
					}
					}
					}
		
				 if (findMatchData.size() == 0)
				{
						String myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						Thread.sleep(1 * 60 * 1000);
						String myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						String myStateQuery = Constants.userStateQuery.replace("__channelId",userId);
						List<String[]> myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						System.out.println("1373");
						if (!myOldChatCount.equalsIgnoreCase(myNewChatCount)||!myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						    return;
						myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						if (myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						{
							System.out.println("1379"); 
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
							String deptClient=userDetails.get(0)[8].toString();
							if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
							 {
									flag=false;
									deptClient=userDetails.get(0)[8].toString();
									finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
									findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
									
							 }
							if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
							{
							 d=nearByAgent(userId);
								flag=false;
    							  System.out.println(d.getChannelId2());
							    if(d.getChannelId2().equalsIgnoreCase("1111111"))
					    		{
					    	       System.out.println("2055");
					    		}
					    else
					    {
					    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
							 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
							 System.out.println("findMatchData1305"+findMatchData);

					    }
							}
						}
					}

				 if (findMatchData.size() == 0)
				{
						String myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						Thread.sleep(1 * 60 * 1000);
						String myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						String myStateQuery = Constants.userStateQuery.replace("__channelId",userId);
						List<String[]> myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						System.out.println("1404");
						if (!myOldChatCount.equalsIgnoreCase(myNewChatCount)||!myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						    return;
						myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						if (myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						{
							System.out.println("1410"); 
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
							String deptClient=userDetails.get(0)[8].toString();
							if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
							 {
									flag=false;
									 deptClient=userDetails.get(0)[8].toString();
									finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
									findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
									
							 }
							if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
							{
							 d=nearByAgent(userId);
							  System.out.println(d.getChannelId2());
								flag=false;
								
							    if(d.getChannelId2().equalsIgnoreCase("1111111"))
					    		{
					    	       System.out.println("2099");
					    		}
					    else
					    {
					    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
							 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
							 System.out.println("findMatchData1305"+findMatchData);

					    }
							}
						}
					}
				 if (findMatchData.size() == 0)
				{
						String myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						Thread.sleep(1 * 60 * 1000);
						String myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						String myStateQuery = Constants.userStateQuery.replace("__channelId",userId);
						List<String[]> myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						System.out.println("1434");
						if (!myOldChatCount.equalsIgnoreCase(myNewChatCount)||!myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						    return;
						myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						if (myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						{
							System.out.println("1440"); 
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
							String deptClient=userDetails.get(0)[8].toString();
							if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
							 {
									flag=false;
									deptClient=userDetails.get(0)[8].toString();
									finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
									findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
									
							 }
							if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
							{
							 d=nearByAgent(userId);
							  System.out.println(d.getChannelId2());
								flag=false;
							    if(d.getChannelId2().equalsIgnoreCase("1111111"))
					    		{
					    	       System.out.println("2142");
					    		}
					    else
					    {
					    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
							 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
							 System.out.println("findMatchData1305"+findMatchData);

					    }
							}
						}
					}

				 if (findMatchData.size() == 0)
				{
						String myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						Thread.sleep(1 * 60 * 1000);
						String myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						String myStateQuery = Constants.userStateQuery.replace("__channelId",userId);
						List<String[]> myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						System.out.println("1465");
						if (!myOldChatCount.equalsIgnoreCase(myNewChatCount)||!myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						    return;
						myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						if (myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						{
							System.out.println("1471"); 
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
							String deptClient=userDetails.get(0)[8].toString();
							if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
							 {
									flag=false;
									deptClient=userDetails.get(0)[8].toString();
									finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
									findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
									
							 }
							if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
							{
							 d=nearByAgent(userId);
								flag=false;
							    if(d.getChannelId2().equalsIgnoreCase("1111111"))
					    		{
					    	       System.out.println("2187");
					    		}
					    else
					    {
					    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
							 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
							 System.out.println("findMatchData1305"+findMatchData);

					    }
							}
						}
					}
				
				 if (findMatchData.size() == 0)
					{
						String myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						Thread.sleep(1 * 60 * 1000);
						String myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
						String myStateQuery = Constants.userStateQuery.replace("__channelId",userId);
						List<String[]> myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						System.out.println("1496");
						if (!myOldChatCount.equalsIgnoreCase(myNewChatCount) || !myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						    return;
						myStateData = DataBaseHandler.getInstance().readData(myStateQuery);
						if (myStateData.get(0)[3].equalsIgnoreCase("waitingForMatch"))
						{
							System.out.println("1502");						    
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
							String deptClient=userDetails.get(0)[8].toString();
							if (userDetails.get(0)[9].equalsIgnoreCase("B2C"))
							 {
									flag=false;
									deptClient=userDetails.get(0)[8].toString();
									finndMatchQuery = Constants.findCustomerCareQuery.replace("__channelId",userId).replace("dept",deptClient);
									findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
									
							 }
							if (userDetails.get(0)[9].equalsIgnoreCase("B2B2C"))
							{
							 d=nearByAgent(userId);
								flag=false;
							    if(d.getChannelId2().equalsIgnoreCase("1111111"))
					    		{
					    	       System.out.println("1971");
					    		}
					    else
					    {
					    	 finndMatchQuery =Constants.findLocalTA.replace("__channelId",userId).replace("nearByAgent",d.getChannelId2());
							 findMatchData = DataBaseHandler.getInstance().readData(finndMatchQuery);
							 System.out.println("findMatchData1305"+findMatchData);

					    }
							}
							if (findMatchData.size() > 0)
							{
								DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
										{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"
												+"fb"+ "'");
								}	
							 else
								{
									myOldChatCount = DataBaseHandler.getInstance().getChatCount(userId);
									DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
											{"CurrentPreference", "UpdateTime"}, Arrays.asList("chat", dateFormat.format(new Date())), "channelId='" + userId + "'");
									Thread.sleep(1 * 20 * 1000);
									myNewChatCount = DataBaseHandler.getInstance().getChatCount(userId);
									List<String> userStateDataValues=new ArrayList();
									userStateDataValues.add(userId);
									userStateDataValues.add("fb");
									userStateDataValues.add("welcomeMessageSent");
									userStateDataValues.add("none");
									userStateDataValues.add(dateFormat.format(new Date()));
						            String userStateColumns[]=Constants.userStateCols.split(",");
									String stateMessage = "";
									stateMessage=Constants.busyMessage;
									sendText(userId,url,stateMessage);
									DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
											{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
									DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
												{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("welcomeMessageSent","none",dateFormat.format(new Date())), "channelId='" +userId + "' and channelType='"									+"fb"+ "'");
									DataBaseHandler.getInstance().updateData(Constants.userDetailsTable, new String[]
												{"UserType"}, Arrays.asList("5"), "channelId='" +userId + "' and channelType='"+"fb"+ "'");		
									String title[]=new String[3];	
				 				    title[0]="New Booking";
				 				    title[1]="Existing Booking";
				 				    title[2]="Agent Connect";
		                 		    String urls[]=new String[3];	
		                 		    urls[0]="http://i67.tinypic.com/smr88i.png";
		                 		    urls[1]="http://i64.tinypic.com/2m2gqpy.png";
		                 		    urls[2]="http://i65.tinypic.com/w9ziup.png";
				 				    System.out.println("799.................");
				 				    sendQuickReplyWithImage(userId,url,Constants.welcomeMsg,title,urls,"menu");
									return;
								}
						  }
				    }
			if (findMatchData.size() > 0)
			{
				System.out.println(findMatchData.get(0)+"1562");
				System.out.println("1563");
				userDataValues.add("matched");
				userDataValues.add("chat");
				userDataValues.add(dateFormat.format(new Date()));
				DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
						{"State","CurrentPreference", "UpdateTime"}, Arrays.asList("matched","chat", dateFormat.format(new Date())), "channelId='" + userId + "' and channelType='"
								+ "fb" + "'");
				System.out.println(findMatchData.get(0)+"1570");
			        	//Change the state of matched user to matched
				System.out.println(findMatchData.get(0)+"320"+findMatchData.get(0)[1]+"319"+findMatchData.get(0)[2]);
				        String userStateColumns[]=Constants.userStateCols.split(",");
						List<String> matchedUserStateDataValues = new ArrayList<String>();
						matchedUserStateDataValues.add(findMatchData.get(0)[1]);
						matchedUserStateDataValues.add(findMatchData.get(0)[2]);
						matchedUserStateDataValues.add("matched");
						matchedUserStateDataValues.add("chat");
						matchedUserStateDataValues.add(dateFormat.format(new Date()));
						matchedUserStateDataValues.add("0");
						DataBaseHandler.getInstance().updateData(Constants.userStateTable,userStateColumns, matchedUserStateDataValues, "channelId='" + findMatchData.get(0)[1] + "' and channelType='"+ findMatchData.get(0)[2] + "'");
				        System.out.println("abcd"+findMatchData.get(0)[1]+"320"+findMatchData.get(0)[2]+"319"+findMatchData.get(0)[3]);
						//add the entry in the matched table
						List<String> matchedUsers = new ArrayList<String>();
						matchedUsers.add(userId);
						matchedUsers.add("fb");
						matchedUsers.add(findMatchData.get(0)[1]);
						matchedUsers.add(findMatchData.get(0)[2]);
						matchedUsers.add("0");
						matchedUsers.add(dateFormat.format(new Date()));
						List<String[]> matchedData = DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId",userId));
						if (matchedData.size() > 0)
						{
							System.out.println("matchedData 344"+" ");
							DataBaseHandler.getInstance().updateData(Constants.matchedUsersTable, Constants.matchedUsersCols.split(","), matchedUsers, "channelId='" +  userId
							+ "' and channelType='" + "fb" + "'");
						}
						else
						{
							System.out.println("matchedData349"+" ");
							DataBaseHandler.getInstance().addData(Constants.matchedUsersTable, Constants.matchedUsersCols.split(","), matchedUsers);
						}

						List<String> matchedUser1 = new ArrayList<String>();
						matchedUser1.add(findMatchData.get(0)[1]);
						matchedUser1.add(findMatchData.get(0)[2]);
						//matchedUser1.add(gscontext.getContextObj().toString());
						matchedUser1.add(userId);
						matchedUser1.add("fb");
						matchedUser1.add("0");
						matchedUser1.add(dateFormat.format(new Date()));
						// Check if entry already exists
						matchedData = DataBaseHandler.getInstance().readData(Constants.matchedUsersQuery.replace("__channelId", findMatchData.get(0)[1]).replace("__channelType", findMatchData.get(0)[2]));
						if (matchedData.size() > 0)
						{
							
							DataBaseHandler.getInstance().updateData(Constants.matchedUsersTable,Constants.matchedUsersCols.split(","), matchedUser1, "channelId='" + findMatchData.get(0)[1]
									+ "' and channelType='" + findMatchData.get(0)[2] + "'");
						} else
						{
							// This is the 1st time the user is chatting
							DataBaseHandler.getInstance().addData(Constants.matchedUsersTable,Constants.matchedUsersCols.split(","), matchedUser1);
						}
											
							if(flag){
							System.out.println("1625");
							userDetails= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",findMatchData.get(0)[0]));
						    String dept=" ";
							if(userDetails.get(0)[8].equalsIgnoreCase("1")){
						    	dept="Flight";
						    }
							if(userDetails.get(0)[8].equalsIgnoreCase("3")){
						    	dept="Hotels";
						    }
						    if(userDetails.get(0)[8].equalsIgnoreCase("1")){
						    	dept="Holidays";
						    }
							
							String messageToBeSent = Constants.matchFoundMessage.replace("<Dept Name>",dept);
							String messageToBeSentAgent = Constants.matchAgent.replace("<customer>",userDetails.get(0)[3] ).replace("<Dept Name>",dept);	
							System.out.println(findMatchData.get(0)[2].toString());
							sendText(userId,url,messageToBeSentAgent);
							sendText(userId,url,"Hi");
							String pageToken=map.get(pageId);
						    pageToken=map.get(userDetails.get(0)[9]);
						    DataBaseHandler.getInstance().updateChatCount(userId);	
							String FB_MSG_URL = "https://graph.facebook.com/v2.6/me/messages?access_token=";
						      url=FB_MSG_URL+map.get(pageId);
							  profileLink=profileLink+map.get(pageId);
							  List<String[]> userDetails1= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",findMatchData.get(0)[0]).replace("__channelType","fb"));
							  url+=map.get(userDetails1.get(0)[10]);
							  System.out.println("User Details 12345"+" "+map.get(userDetails1.get(0)[9]));
							  url="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetails1.get(0)[10]);
							  sendText(findMatchData.get(0)[1],url,messageToBeSent);
							  sendText(findMatchData.get(0)[1],url,"Hi");
							}
							else{
								System.out.println("1658");
								userDetails= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",userId).replace("__channelType","fb"));
								if(userDetails.get(0)[8].equalsIgnoreCase("1")){
							    	dept="Flight";
							    }
								if(userDetails.get(0)[8].equalsIgnoreCase("3")){
							    	dept="Hotels";
							    }
							    if(userDetails.get(0)[8].equalsIgnoreCase("7")){
							    	dept="Holidays";
							    }
						       String messageToBeSent = Constants.matchFoundMessage;
							   System.out.println(userDetails.get(0)[2]);
							   String messageToBeSentAgent = Constants.matchAgent.replace("<customer>",userDetails.get(0)[3]);	
							   System.out.println(messageToBeSent);
							   System.out.println(messageToBeSentAgent);
							   System.out.println(userId);
							   DataBaseHandler.getInstance().updateChatCount(userId);	
							   System.out.println(findMatchData.get(0)[1]);
							   List<String[]> userDetails1= DataBaseHandler.getInstance().readData(Constants.userDetailsQuery.replace("__channelId",findMatchData.get(0)[1]).replace("__channelType","fb"));
							   sendText(userId,url,messageToBeSent);
							   sendText(userId,url,"Hi");
							   System.out.println("User Details 1234567"+" "+findMatchData.get(0)[1]);
							   System.out.println("User Details 123"+userDetails1.get(0)[10]);
							   System.out.println("User Details 12345"+" "+map.get(userDetails1.get(0)[10]));
							   url="https://graph.facebook.com/v2.6/me/messages?access_token="+map.get(userDetails1.get(0)[10]);
							   sendText(findMatchData.get(0)[1],url,messageToBeSentAgent);	
							   sendText(findMatchData.get(0)[1],url,"Hi");
			                 }	
							String id=DataBaseHandler.getInstance().addData_advice(Constants.matchHistoryTable, Constants.matchHistoryCols.split(","), Arrays.asList(userId, findMatchData.get(0)[1], "CHAT",dateFormat.format(new Date())));
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{ "matchID" }, Arrays.asList(id), "channelId='" + userId+ "'");
							DataBaseHandler.getInstance().updateData(Constants.userStateTable, new String[]
									{ "matchID" }, Arrays.asList(id), "channelId='" + findMatchData.get(0)[1] + "'");
	                    }
			return;
	  }
		         }
			}
		
			catch(Exception e)
		{
			e.printStackTrace();
			logExceptionInDB(userId,"fb",text,e);
			}
	
	}
	
	
	private void sendText( String userId,String url,String text){
		Example_Text exampleText=new Example_Text();	
		Recipient_Text receipentText=new Recipient_Text();
		receipentText.setId(userId);
		exampleText.setRecipient(receipentText);
		Message_Text messageText=new Message_Text();
		messageText.setText(text);
		exampleText.setMessage(messageText);
		Gson gson = new Gson();
		String sendMessage=gson.toJson(exampleText); 
		System.out.println("Text Message"+sendMessage);
		sendTextMessage(sendMessage,true,url);
		}
		
		    
	
	private void sendAudio( String userId,String url,String audioUrl){
		ExampleSend exmpleImage=new ExampleSend();
		RecipientSend receipt=new RecipientSend();
		receipt.setId(userId);
		exmpleImage.setRecipient(receipt);
		MessageSend messageImageReply=new MessageSend();
		AttachmentSend attachment=new AttachmentSend();	
		attachment.setType("audio");
		Payload payload=new Payload();
		payload.setUrl(audioUrl);
		messageImageReply.setAttachment(attachment);
		attachment.setPayload(payload);
		exmpleImage.setMessage(messageImageReply);
		Gson gsonRsp = new Gson();
		String sendMsgs=gsonRsp.toJson(exmpleImage); 
		System.out.println("Text Message"+sendMsgs);
		sendTextMessage(sendMsgs,true,url);
		}
	
	  private void sendFile( String userId,String url,String fileUrl){
		ExampleFile exmpleFile=new ExampleFile();
		RecipientFile receipt=new RecipientFile();
		receipt.setId(userId);
		exmpleFile.setRecipient(receipt);
		MessageFile messageReply=new MessageFile();
		AttachmentFile attachment=new AttachmentFile();	
		attachment.setType("file");
		PayloadFile payload=new PayloadFile();
	     payload.setUrl(fileUrl);
		messageReply.setAttachment(attachment);
		attachment.setPayload(payload);
		exmpleFile.setMessage(messageReply);
		Gson gsonRsp = new Gson();
	    String sendMsgs=gsonRsp.toJson(exmpleFile); 
		System.out.println("Text Message"+sendMsgs);
		sendTextMessage(sendMsgs,true,url);		
		}
	
	private void sendImage( String userId,String url,String urlImage)
	{
		ExampleSend exmpleImage=new ExampleSend();
		RecipientSend receipt=new RecipientSend();
		receipt.setId(userId);
		exmpleImage.setRecipient(receipt);
		MessageSend messageImageReply=new MessageSend();
		AttachmentSend attachment=new AttachmentSend();	
		attachment.setType("image");
		Payload payload=new Payload();
		payload.setUrl(urlImage);
		messageImageReply.setAttachment(attachment);
		attachment.setPayload(payload);
		exmpleImage.setMessage(messageImageReply);
		Gson gsonRsp = new Gson();
		String sendMsgs=gsonRsp.toJson(exmpleImage); 
		System.out.println("Text Message"+sendMsgs);
		sendTextMessage(sendMsgs,true,url);

			}
	  public boolean isValidEmailAddress(String email) {
          String ePattern = "^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@((\\[[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\.[0-9]{1,3}\\])|(([a-zA-Z\\-0-9]+\\.)+[a-zA-Z]{2,}))$";
          java.util.regex.Pattern p = java.util.regex.Pattern.compile(ePattern);
          java.util.regex.Matcher m = p.matcher(email);
          return m.matches();
   }
	  private void sendQuickReply( String userId,String url,String text,String title[],String payload){
		Example_Quick_Replies quickReply=new Example_Quick_Replies();
		Recipient_Quick_reply receiptentQuickReply=new Recipient_Quick_reply();
		receiptentQuickReply.setId(userId);
		quickReply.setRecipient(receiptentQuickReply);
		Message_Quick_Reply messageQuickReply=new Message_Quick_Reply();
		messageQuickReply.setText(text);
		quickReply.setMessage(messageQuickReply);		
		List<QuickReply> quick=new ArrayList<QuickReply>();
		int i;
		j=0;
		for(i=0;i<title.length;i++){
		QuickReply quickReplyText=new QuickReply();
		quickReplyText.setContentType("text");
		quickReplyText.setTitle(title[i]);
		//quickReplyText.setImageUrl();
		System.out.println(title[i]);
		quickReplyText.setPayload(payload);
		quick.add(quickReplyText);		
		}
		messageQuickReply.setQuickReplies(quick);
		Gson gsonResponse = new Gson();
		String sendMsg=gsonResponse.toJson(quickReply); 
		System.out.println("Text Message"+sendMsg);
		sendTextMessage(sendMsg,true,url);
			}
	  private void sendQuickReplyLocation( String userId,String url){
		  isCity=true;
		ExampleLocationQR quickReply=new ExampleLocationQR();
		RecipientLocationQR receiptentQuickReply=new RecipientLocationQR();
		receiptentQuickReply.setId(userId);
		quickReply.setRecipient(receiptentQuickReply);
		MessageLocationQR messageQuickReply=new MessageLocationQR();
		messageQuickReply.setText("Please share your location:");
		quickReply.setMessage(messageQuickReply);		
		List<QuickReplyLocation> quick=new ArrayList<QuickReplyLocation>();
		int i;
		j=0;
		QuickReplyLocation quickReplyText=new QuickReplyLocation();
		quickReplyText.setContentType("location");
		quick.add(quickReplyText);		
		messageQuickReply.setQuickReplies(quick);
		Gson gsonResponse = new Gson();
		String sendMsg=gsonResponse.toJson(quickReply); 
		System.out.println("Text Message"+sendMsg);
		sendTextMessage(sendMsg,true,url);
			}


	  private void sendLocation( String userId,String url,Double lat,Double longitude,String pageId,Long time,Long timeStamp,String mid,Long seq)
	  {
		ExampleLocation  exmpleLocation=new ExampleLocation();
		exmpleLocation.setObject("page");
		List<EntryLocation> entry=new ArrayList<EntryLocation>();
		EntryLocation entryLoc=new EntryLocation();
		entryLoc.setId(pageId);
		entryLoc.setTime(time);
		List<MessagingLocation> msgLoc=new ArrayList<MessagingLocation>();
		MessagingLocation msgLocation=new MessagingLocation();
		msgLocation.setTimestamp(timeStamp);
		entry.add(entryLoc);
		SenderLocation senderLoc= new SenderLocation();
		senderLoc.setId(userId);
		RecipientLocation recipentLoc=new RecipientLocation();
		recipentLoc.setId(pageId);
		MessageLocation msgingLocation=new MessageLocation();
		msgingLocation.setMid(mid);
		msgingLocation.setSeq(seq);
		msgLoc.add(msgLocation);
		List<AttachmentLocation> attLoc=new ArrayList<AttachmentLocation>();
		AttachmentLocation attLocation= new AttachmentLocation();
		attLocation.setTitle("Location");
		attLocation.setUrl(url);
		attLocation.setType("location");
		PayloadLocation payloadLoc= new PayloadLocation();
		CoordinatesLocation coordLoc=new CoordinatesLocation();
		coordLoc.setLat(lat);
		coordLoc.setLong(longitude);
		payloadLoc.setCoordinates(coordLoc);
		attLoc.add(attLocation);
		msgingLocation.setAttachments(attLoc);
		exmpleLocation.setEntry(entry);
		Gson gsonResponse = new Gson();
		String sendMsg=gsonResponse.toJson(exmpleLocation); 
		System.out.println("Text Message"+sendMsg);
		sendTextMessage(sendMsg,true,url);
			}
	  
	  private void sendQuickReplyWithImage( String userId,String url,String text,String title[],String urls[],String payload)
	  {
		Example_Quick_Replies quickReply=new Example_Quick_Replies();
		Recipient_Quick_reply receiptentQuickReply=new Recipient_Quick_reply();
		receiptentQuickReply.setId(userId);
		quickReply.setRecipient(receiptentQuickReply);
		Message_Quick_Reply messageQuickReply=new Message_Quick_Reply();
		messageQuickReply.setText(text);
		quickReply.setMessage(messageQuickReply);		
		List<QuickReply> quick=new ArrayList<QuickReply>();
		int i;
		j=0;
		for(i=0;i<title.length;i++){
		QuickReply quickReplyText=new QuickReply();
		quickReplyText.setContentType("text");
		quickReplyText.setTitle(title[i]);
		quickReplyText.setImageUrl(urls[i]);
		System.out.println(title[i]);
		quickReplyText.setPayload(payload);
		quick.add(quickReplyText);		
		}
		messageQuickReply.setQuickReplies(quick);
		Gson gsonResponse = new Gson();
		String sendMsg=gsonResponse.toJson(quickReply); 
		System.out.println("Text Message"+sendMsg);
		sendTextMessage(sendMsg,true,url);
			}

	  private void sendCatelog( String userId,String url,String text,String title[],String payload[]){
		ExampleCatelog catelog=new ExampleCatelog();
		RecipientCatelog receiptentcatelog=new RecipientCatelog();
		receiptentcatelog.setId(userId);
		catelog.setRecipient(receiptentcatelog);
		MessageCatelog messageCatelog=new MessageCatelog();
		AttachmentCatelog attachmentCatelog=new AttachmentCatelog();
		attachmentCatelog.setType("template");
		PayloadCatelog payloadcatelog=new PayloadCatelog();
		payloadcatelog.setTemplateType("button");
		payloadcatelog.setText(text);
		attachmentCatelog.setPayload(payloadcatelog);
		messageCatelog.setAttachment(attachmentCatelog);
		catelog.setMessage(messageCatelog);
		List<ButtonCatelog> button=new ArrayList<ButtonCatelog>();
		int i;
		j=0;
		for(i=0;i<title.length;i++){
		ButtonCatelog buttonCatelog=new ButtonCatelog();
		buttonCatelog.setType("postback");
		buttonCatelog.setTitle(title[i]);
		buttonCatelog.setPayload(payload[i]);
		System.out.println(title[i]);
		button.add(buttonCatelog);		
		}
		payloadcatelog.setButtons(button);
		Gson gsonResponse = new Gson();
		String sendMsg=gsonResponse.toJson(catelog); 
		System.out.println("Text Message"+sendMsg);
		sendTextMessage(sendMsg,true,url);
			}
	  
	      private void sendTextMessage( String text,boolean isPostBack,String url) {	
			try {				
				OkHttpClient httpClient= new OkHttpClient();
				RequestBody body = RequestBody.create(MediaType.parse("application/json; charset=utf-8"), text);
				Request request = new Request.Builder()
						.url(url)
						.header("Content-Type", "application/json; charset=UTF-8")
						.post(body)
						.build();
				okhttp3.Response response = httpClient.newCall(request).execute();
				final int code = response.code();
				System.out.println("Code"+code);
				String results = response.body().string();
				System.out.println("Result Response"+results);
				Response returnValue = null;
				System.out.println("Send Text Message Function called");
				if (code == 200) {
					System.out.println("Send Text Message Function called code 200");
					
				}else {
					System.out.println("Send Text Message Function called other code");
					
				}
				System.out.println("");
				response.body().close();
			} catch (UnsupportedEncodingException e) {
				logExceptionInDB(userId,"fb","Error message",e);
				e.printStackTrace();
			} catch (ClientProtocolException e) {
				logExceptionInDB(userId,"fb","Error message",e);
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
				logExceptionInDB(userId,"fb","Error message",e);
			}
	}
	
	private <T> T getObjectFromUrl(String link, Class<T> clazz) {
		T t = null;
		URL url;
		String jsonString = "";
		try {
			url = new URL(link);
			BufferedReader in = new BufferedReader(new InputStreamReader(
					url.openStream()));

			String inputLine;
			while ((inputLine = in.readLine()) != null) {
				jsonString = jsonString + inputLine;
			}
			in.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			logExceptionInDB(userId,"fb","Error message",e);
			e.printStackTrace();
		}
		if (!StringUtils.isEmpty(jsonString)) {
			Gson gson = new Gson();
			t = gson.fromJson(jsonString, clazz);
		}
		return t;
	}
	@Override
	public void destroy() {
		System.out.println("webhook Servlet Destroyed");
	}

	@Override
	public void init() throws ServletException {

		System.out.println("webhook servlet created!!");
	}
	}

