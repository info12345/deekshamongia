package in.strollup.fb.contract;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExampleSend {

    @SerializedName("recipient")
    @Expose
    private RecipientSend recipient;
    @SerializedName("message")
    @Expose
    private MessageSend message;

    public RecipientSend getRecipient() {
        return recipient;
    }

    public void setRecipient(RecipientSend recipient) {
        this.recipient = recipient;
    }

    public MessageSend getMessage() {
        return message;
    }

    public void setMessage(MessageSend message) {
        this.message = message;
    }

}
