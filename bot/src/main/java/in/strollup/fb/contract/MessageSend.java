package in.strollup.fb.contract;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageSend {

    @SerializedName("attachment")
    @Expose
    private AttachmentSend attachment;

    public AttachmentSend getAttachment() {
        return attachment;
    }

    public void setAttachment(AttachmentSend attachment) {
        this.attachment = attachment;
    }

}
