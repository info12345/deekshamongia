package in.strollup.fb.contract;

import java.util.List;
import javax.annotation.Generated;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Generated("org.jsonschema2pojo")
public class Message_Quick_Reply {
	 @SerializedName("text")
	    @Expose
	    private String text;
	    @SerializedName("quick_replies")
	    @Expose
	    private List<QuickReply> quickReplies = null;

	    public String getText() {
	        return text;
	    }

	    public void setText(String text) {
	        this.text = text;
	    }

	    public List<QuickReply> getQuickReplies() {
	        return quickReplies;
	    }

	    public void setQuickReplies(List<QuickReply> quickReplies) {
	        this.quickReplies = quickReplies;
	    }
}
