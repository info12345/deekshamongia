package in.strollup.fb.contract;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MessageCatelog {

    @SerializedName("attachment")
    @Expose
    private AttachmentCatelog attachment;

    public AttachmentCatelog getAttachment() {
        return attachment;
    }

    public void setAttachment(AttachmentCatelog attachment) {
        this.attachment = attachment;
    }

}
