package in.strollup.fb.contract;


import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ExampleCatelog {

    @SerializedName("recipient")
    @Expose
    private RecipientCatelog recipient;
    @SerializedName("message")
    @Expose
    private MessageCatelog message;

    public RecipientCatelog getRecipient() {
        return recipient;
    }

    public void setRecipient(RecipientCatelog recipient) {
        this.recipient = recipient;
    }

    public MessageCatelog getMessage() {
        return message;
    }

    public void setMessage(MessageCatelog message) {
        this.message = message;
    }

}
