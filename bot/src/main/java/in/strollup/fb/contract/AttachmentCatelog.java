package in.strollup.fb.contract;



import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class AttachmentCatelog {

    @SerializedName("type")
    @Expose
    private String type;
    @SerializedName("payload")
    @Expose
    private PayloadCatelog payload;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public PayloadCatelog getPayload() {
        return payload;
    }

    public void setPayload(PayloadCatelog payload) {
        this.payload = payload;
    }

}
