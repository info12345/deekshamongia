
package in.strollup.fb.contract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example_Quick_Replies {

    @SerializedName("recipient")
    @Expose
    private Recipient_Quick_reply recipient;
    @SerializedName("message")
    @Expose
    private Message_Quick_Reply message;

    public Recipient_Quick_reply getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient_Quick_reply recipient) {
        this.recipient = recipient;
    }

    public Message_Quick_Reply getMessage() {
        return message;
    }

    public void setMessage(Message_Quick_Reply message) {
        this.message = message;
    }

}
