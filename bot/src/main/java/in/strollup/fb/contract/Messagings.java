
package in.strollup.fb.contract;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Messagings {

    @SerializedName("sender")
    @Expose
    private Sender sender;
    @SerializedName("recipient")
    @Expose
    private Recipient recipient;
    @SerializedName("timestamp")
    @Expose
    private Integer timestamp;
    @SerializedName("message")
    @Expose
    private Message_Quick_Reply message;

    public Sender getSender() {
        return sender;
    }

    public void setSender(Sender sender) {
        this.sender = sender;
    }

    public Recipient getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient recipient) {
        this.recipient = recipient;
    }

    public Integer getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Integer timestamp) {
        this.timestamp = timestamp;
    }

    public Message_Quick_Reply getMessage() {
        return message;
    }

    public void setMessage(Message_Quick_Reply message) {
        this.message = message;
    }

}
