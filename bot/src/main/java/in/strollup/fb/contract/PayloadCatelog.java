package in.strollup.fb.contract;



import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class PayloadCatelog {

    @SerializedName("template_type")
    @Expose
    private String templateType;
    @SerializedName("text")
    @Expose
    private String text;
    @SerializedName("buttons")
    @Expose
    private List<ButtonCatelog> buttons = null;

    public String getTemplateType() {
        return templateType;
    }

    public void setTemplateType(String templateType) {
        this.templateType = templateType;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public List<ButtonCatelog> getButtons() {
        return buttons;
    }

    public void setButtons(List<ButtonCatelog> buttons) {
        this.buttons = buttons;
    }

}
