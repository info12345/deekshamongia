
package in.strollup.fb.contract;
import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Messages {

    @SerializedName("mid")
    @Expose
    private String mid;
    @SerializedName("seq")
    @Expose
    private Integer seq; 
    @SerializedName("text")
    @Expose
    private String text;
   @SerializedName("attachments")
    @Expose
    private List<Attachment> attachment;

    public List<Attachment> getAttachment() {
        return attachment;
    }

    public void setAttachments(List<Attachment> attachments) {
        this.attachment = attachment;
    }


    public String getMid() {
        return mid;
    }

    public void setMid(String mid) {
        this.mid = mid;
    }

    public Integer getSeq() {
        return seq;
    }

    public void setSeq(Integer seq) {
        this.seq = seq;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

}
