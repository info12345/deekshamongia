package in.strollup.fb.contract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example_Text {

    @SerializedName("recipient")
    @Expose
    private Recipient_Text recipient;
    @SerializedName("message")
    @Expose
    private Message_Text message;

    public Recipient_Text getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient_Text recipient) {
        this.recipient = recipient;
    }

    public Message_Text getMessage() {
        return message;
    }

    public void setMessage(Message_Text message) {
        this.message = message;
    }


}
