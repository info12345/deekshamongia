package in.strollup.fb.contract;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example_Image {

    @SerializedName("recipient")
    @Expose
    private Recipient_Image recipient;
    @SerializedName("message")
    @Expose
    private Messages message;

    public Recipient_Image getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient_Image recipient) {
        this.recipient = recipient;
    }

    public Messages getMessage() {
        return message;
    }

    public void setMessage(Messages message) {
        this.message = message;
    }

}
