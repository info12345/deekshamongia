package in.strollup.fb.contract;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Example_Audio {

    @SerializedName("recipient")
    @Expose
    private Recipient_Audio recipient;
    @SerializedName("message")
    @Expose
    private Messages message;

    public Recipient_Audio getRecipient() {
        return recipient;
    }

    public void setRecipient(Recipient_Audio recipient) {
        this.recipient = recipient;
    }

    public Messages getMessage() {
        return message;
    }

    public void setMessage(Messages message) {
        this.message = message;
    }

}
